<?php 
/* Aanbod - gefilterd overzicht */ 
get_header(); 

$aanbod = AanbodService::i();
$tax_query = [];
if ( $aanbod->hasFilter('filter-locaties') ) { array_push($tax_query, array( 'taxonomy' => 'locaties', 'field' => 'term_id', 'terms' => $aanbod->getFilter('filter-locaties') ) ); }
if ( $aanbod->hasFilter('filter-leeftijden') ) { array_push($tax_query, array( 'taxonomy' => 'leeftijden', 'field' => 'term_id', 'terms' => $aanbod->getFilter('filter-leeftijden') ) ); }
if ( $aanbod->hasFilter('filter-groepen') ) { array_push($tax_query, array( 'taxonomy' => 'groepen', 'field' => 'term_id', 'terms' => $aanbod->getFilter('filter-groepen') ) ); }

$aanbodQuery = new WP_query( array( 'post_type' => 'cursussen', 'posts_per_page' => -1, 'tax_query' => $tax_query ) );
?>

<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__header__visual__wrapper">
					<button class="container__header__back" onclick="window.history.go(-1);"><i class="ani__all fa fa-chevron-left fa-fw"></i></button>
					<?php echo wp_get_attachment_image( get_field( 'global-aanbod-header-visual','option' ), 'page-header-visual', false, array( 'class' => 'container__header__visual' ) ); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="block__category__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__row__wrapper no-gutter">
					<div class="col-md-6">												
						<div class="block__category__title">
							<h2 class="title title--light title--medium title--large"><?php the_field( 'global-aanbod-header-title', 'option' ); ?><br/><?php echo $aanbodQuery->post_count; ?>x</h2>
						</div>
					</div>
					<div class="col-md-6">
						<div class="block__category__text">
							<?php the_field( 'global-aanbod-header-text', 'option' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-4">
					<div class="block__aanbod block__aanbod--mini matchheight">
						<h2 class="title title--light title--small">Ons aanbod</h2>
						<button class="comp__start__finder"><i class="fa fa-search ani__all"></i><span class="ani__all">Zoek opnieuw</span></button>
					</div>
					<?php get_template_part( 'components/site', 'cursus-finder' ); ?>
				</div>	
				
				<?php
				while ( $aanbodQuery->have_posts() ) { 
					$aanbodQuery->the_post();
					?>
					<div class="col-md-4">
						<a class="loop__cursus__item matchheight" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<h3 class="title title--light title--shadow title--normal loop__cursus__title"><?php echo bwh_cursus_title( get_the_title() ); ?></h3>
							<?php
							if ( has_post_thumbnail() ) { echo get_the_post_thumbnail(get_the_ID(), 'cursus-loop-thumbnail', array( 'class' => 'loop__cursus__thumbnail ani__all' ) ); }
							else { echo wp_get_attachment_image( get_field( 'global-aanbod-default-poster', 'option' ), 'cursus-loop-thumbnail', false, array( 'class' => 'loop__cursus__thumbnail ani__all' ) ); }
							?>
						</a>
					</div>	
					<?php
				} wp_reset_query();
				?>
			</div>
		</div>
	</div>
</div>

<?php get_template_part( 'components/site', 'highlights' ); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">&nbsp;</div>		
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>