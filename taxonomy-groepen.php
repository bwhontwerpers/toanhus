<?php 
	/* Taxonomy - groepen */ 
	get_header();

	$queried_id = $wp_query->get_queried_object_id();
	$aanbod = AanbodService::i();
	$groep = $aanbod->setCache($queried_id);

	include( locate_template( 'components/site-aanbod-header-visual.php', false, false)); ?>
	<div class="aanbod-content-wrapper"> 
	<?php
		include( locate_template( 'components/site-aanbod-header-content.php', false, false));
		include( locate_template( 'components/site-aanbod-cursussen.php', false, false));
		if( $groep->is_parent ) include( locate_template( 'components/site-aanbod-groepen.php', false, false));
		get_template_part( 'components/site', 'close-whiteblock' );
	?>
	</div>
<?php
	get_footer(); 
?>