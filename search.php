<?php 
/* Search */ 
get_header();
global $wp_query;
?>

<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="container__header__visual__wrapper">
					<button class="container__header__back" onclick="window.history.go(-1);"><i class="ani__all fa fa-chevron-left fa-fw"></i></button>
					<?php echo wp_get_attachment_image( get_field( 'global-groepen-header-visual','option' ), 'page-header-visual', false, array( 'class' => 'container__header__visual' ) ); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="block__category__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__row__wrapper no-gutter">
					<div class="col-md-6 col-sm-6">												
						<div class="block__category__title">
							<h2 class="title title--light title--medium title--large"><?php the_search_query(); ?><br/><?php echo $wp_query->post_count; ?>x</h2>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="block__category__text">
							<?php echo str_replace( array( '{{totaal}}', '{{zoekterm}}'), array( $wp_query->post_count, get_search_query() ), get_field( 'global-search-intro', 'option' ) ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">				
				<?php
				while ( have_posts() ) {
					the_post();
					?>
					<div class="col-md-4 col-sm-6">
						<a class="loop__cursus__item matchheight" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<h3 class="title title--light title--shadow title--normal loop__cursus__title"><?php echo bwh_cursus_title( get_the_title() ); ?></h3>
							<?php
							if ( has_post_thumbnail() ) { echo get_the_post_thumbnail(get_the_ID(), 'cursus-loop-thumbnail', array( 'class' => 'loop__cursus__thumbnail ani__all' ) ); }
							else { echo wp_get_attachment_image( get_field( 'global-aanbod-default-poster', 'option' ), 'cursus-loop-thumbnail', false, array( 'class' => 'loop__cursus__thumbnail ani__all' ) ); }
							?>
						</a>
					</div>	
					<?php
				} wp_reset_query();
				?>
			</div>
		</div>
	</div>
</div>

<div class="container container__contains__finder">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper no-gutter">
				<div class="col-md-6">
					<div class="block__intro matchheight">
						<?php the_field( 'global-cursusfinder-text', 'option' ); ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="block__aanbod matchheight">
						<h2 class="title title--light"><?php the_field( 'global-cursusfinder-title', 'option' ); ?></h2>
						<button class="comp__start__finder"><i class="fa fa-search ani__all"></i><span class="ani__all"><?php the_field( 'global-cursusfinder-button', 'option' ); ?></span></button>
					</div>
					<?php get_template_part( 'components/site', 'cursus-finder' ); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_template_part( 'components/site', 'highlights' ); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">&nbsp;</div>		
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>