<?php 
/* Category - nieuws */ 
get_header();
?>

<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__header__article__container">
				<?php
				$highlightArticleQuery = new WP_Query( array( 'post_type' => 'post', 'cat' => 1, 'posts_per_page' => 1, 'no_found_rows' => true, 'update_post_term_cache' => false, 'update_post_meta_cache' => false, ) );
				while ( $highlightArticleQuery->have_posts() ) { 
					$highlightArticleQuery->the_post();
					?>
					<a class="large__article__item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<h3 class="title title--light title--shadow title--normal title--semilarge large__article__title"><?php echo bwh_cursus_title( get_the_title() ); ?></h3>
						<?php
						if ( has_post_thumbnail() ) { echo get_the_post_thumbnail(get_the_ID(), 'page-header-visual', array( 'class' => 'loop__article__thumbnail ani__all' ) ); }
						else { echo wp_get_attachment_image( get_field( 'global-aanbod-default-poster', 'option' ), 'page-header-visual', false, array( 'class' => 'loop__article__thumbnail ani__all' ) ); }
						?>
						<div class="large__article__description ani__all">
							<time class="large__article__date ani__all" datetime="<?php echo date_i18n( 'd/m/Y', strtotime( $post->post_date ), true ); ?>"><?php echo date_i18n( 'd/m/y', strtotime( $post->post_date ), true ); ?></time>
							<?php the_excerpt(); ?>
						</div>
					</a>
					<?php
				} wp_reset_query();
				?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">			
				<?php
				while ( have_posts() ) { 
					the_post();
					?>
					<div class="col-md-6">
						<a class="loop__article__item matchheight" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<time class="loop__article__date title--shadow" datetime="<?php echo date_i18n( 'd/m/Y', strtotime( $post->post_date ), true ); ?>"><?php echo date_i18n( 'd/m/y', strtotime( $post->post_date ), true ); ?></time>
							<h3 class="title title--light title--shadow title--normal title--semilarge loop__article__title"><?php echo bwh_cursus_title( get_the_title() ); ?></h3>
							<?php
							if ( has_post_thumbnail() ) { echo get_the_post_thumbnail(get_the_ID(), 'cursus-loop-thumbnail', array( 'class' => 'loop__article__thumbnail ani__all' ) ); }
							else { echo wp_get_attachment_image( get_field( 'global-aanbod-default-poster', 'option' ), 'cursus-loop-thumbnail', false, array( 'class' => 'loop__article__thumbnail ani__all' ) ); }
							?>
							<div class="loop__article__description">
								<?php the_excerpt(); ?>
							</div>
						</a>
					</div>	
					<?php
				} wp_reset_query();
				?>
			</div>
		</div>
	</div>
</div>

<?php get_template_part( 'components/site', 'highlights' ); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">&nbsp;</div>		
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>