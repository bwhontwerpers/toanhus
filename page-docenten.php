<?php
/* Single cursus template */
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { $ajax = true; } else { $ajax = false; }
if ($ajax != true) { get_header(); }
?>

<?php while ( have_posts() ) { the_post(); ?>

<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__header__visual__wrapper">
					<button class="container__header__back" onclick="window.history.go(-1);"><i class="ani__all fa fa-chevron-left fa-fw"></i></button>
					<?php
					if ( get_field( 'header-visual' ) ) { $header = get_field( 'header-visual' ); $header = $header['ID']; } 
					else { $header = get_field( 'global-article-header', 'option' ); }
					echo wp_get_attachment_image( $header, 'page-header-visual', false, array( 'class' => 'container__header__visual' ) );
					?>
					<h2 class="news__article__title"><?php echo bwh_cursus_title( get_the_title() ); ?></h2>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container page-content-wrapper-single">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">
					<div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2 teacher-grid">
           
            
              <?php 
                $meta = get_fields();
                //print_r($meta);
              ?>


                <?php 
                  if( isset($meta['add-teacher']) && !empty($meta['add-teacher']) ) : 
                    foreach($meta['add-teacher'] as $teacher ) : ?>

                      <div class="col-xs-12 col-sm-6 teacher-block">
                        <div class="thumbnail">
                          <img src="<?= $teacher['teacher-photo']['sizes']['large']; ?>" alt="<?= $teacher['teacher-name']; ?>">
                        </div>
                        <div class="details">
                          <h3>
                            <?= $teacher['teacher-name']; ?>
                          </h3>
                          <?= $teacher['teacher-info']; ?>
                        </div>
                      </div>

                  <?php 
                    endforeach; ?>
                <?php 
                  endif; ?>

            </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php } ?>

<?php if ($ajax != true) { get_footer(); } ?>