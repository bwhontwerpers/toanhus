<?php
	// Add Services
	// Use AanbodService::i()->somefunction()
	require_once('api/php/aanbod-service.php');
	require_once('api/php/tarieven-instellingen.php');

	/* 
	Set globals (no need to change by default) 
	*/
	$dir = wp_upload_dir();
	define("UPLOADSFOLDER", $dir['url']);
	define("THEMEFOLDER", 	get_bloginfo('stylesheet_directory'));
	define("IMAGEFOLDER", 	get_bloginfo('stylesheet_directory').'/lib/img');
	define("IMAGEPATH",		get_template_directory().'/lib/img');
	define("FONTSFOLDER", 	get_bloginfo('stylesheet_directory').'/lib/fonts');
	define("PHPFOLDER",   	get_theme_root().'/'.get_template().'/api/php');
	define("RESPONSIVEURL", get_bloginfo('stylesheet_directory').'/api/php/image.ajax.php');
	define("PHPURL",      	get_bloginfo('stylesheet_directory').'/api/php');
	define("JSFOLDER",    	get_bloginfo('stylesheet_directory').'/api/js');
	define("HOMEURL", 	  	site_url());
	define("PAGERROR",	  	80);
	define("PAGEMAPS",    	12);
	define("PAGEHOME",    	6);
	define("TEXTDOMAIN",  	'toanhus');
	
	/*
	Frequenties mappings
	*/
	global $frequenties;
	$frequenties = array( '1wk' => '1x per week', '2wk' => '1x per 2 weken', '4wk' => '1x per 4 weken' );
	
	/*
	Lestijden mappings
	*/
	global $tijden;
	$tijden = array( ',50' => '30 minuten', ',75' => '45 minuten', '1' => '1 uur', '1,5' => '1,5 uur', '1,75' => '1,75 uur', '2' => '2 uur', '2,5' => '2,5 uur' );
	
	/*
	Dagen mappings
	*/
	global $weekdagen;
	$weekdagen = array( 'Maandag' => 0, 'Dinsdag' => 1, 'Woensdag' => 2, 'Donderdag' => 3, 'Vrijdag' => 4, 'Zaterdag' => 5, 'Zondag' => 6 );
	
	/* 
	Load external files
	*/
	add_action( 'admin_enqueue_scripts', 'bwh_load_files' );
	add_action( 'wp_enqueue_scripts', 'bwh_load_files' );
	function bwh_load_files() {
		if (!is_admin() || (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) {
			$scriptversion = '1.1.13';
			// wp_enqueue_script('bwh-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCHnEKFPh6XW0nMyNH7A6X-UCrgu1hg6rE', array('jquery'), false, true);
			// wp_enqueue_script( 'bwh-videoheader', JSFOLDER.'/video-header.js', array('jquery'), $scriptversion, true);
			wp_enqueue_script( 'bwh-aanbodproces', JSFOLDER.'/toanhus-aanbod-proces.js', array('jquery'), $scriptversion, true);
			wp_enqueue_script( 'bwh-calculator', JSFOLDER.'/toanhus-tarieven-calculator.js', array('jquery'), $scriptversion, true);
			wp_enqueue_script( 'bwh-functions', JSFOLDER.'/functions.js', array('jquery'), $scriptversion, true);
			wp_enqueue_script( 'bwh-matchheight', JSFOLDER.'/vendor/match.height.min.js', array('jquery'), false, true);
			wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js', array('jquery'), false, true);
			wp_enqueue_script( 'clipboard', '//cdn.jsdelivr.net/clipboard.js/1.5.8/clipboard.min.js', array('jquery'), false, true);
			wp_enqueue_script( 'waypoints', '//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.min.js', array('jquery'), false, true);
			
			wp_register_style( 'fontscom', '//fast.fonts.net/cssapi/8741b4fc-3b57-4a3c-8cdc-bf50351a1c22.css' );
			wp_register_style( 'default', THEMEFOLDER.'/style.css?v='.$scriptversion );
			wp_register_style( 'default-css', THEMEFOLDER.'/styles/main.css?v='.$scriptversion );
			wp_register_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' );
			wp_register_style( 'slick', '//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css' );
			
	        wp_enqueue_style( 'fontscom' );
	        wp_enqueue_style( 'bootstrap' );
					wp_enqueue_style( 'default' );
					wp_enqueue_style( 'default-css' );
	        wp_enqueue_style( 'slick' );
	        	        
		} else {
		    add_editor_style( 'editor-style.css' );			
		}
	}
	
	/* 
	Configure WP menu holders 
	*/
	register_nav_menu( 'topleft', 	__( 'Boven (links)', TEXTDOMAIN ) );
	register_nav_menu( 'topright',  __( 'Boven (rechts)', TEXTDOMAIN ) );
	register_nav_menu( 'bottomright',  __( 'Onder (rechts)', TEXTDOMAIN ) );
	register_nav_menu( 'mobile', 	__( 'Mobiel', TEXTDOMAIN ) );
	register_nav_menu( 'about', 	__( 'Over ons', TEXTDOMAIN ) );

	/*
	Enable post thumbnails support
	*/
	add_theme_support( 'post-thumbnails' );
	
	/*
	Configure image sizes
	*/
	add_image_size( 'cursus-slider-visual', 1020, 558, true );
	add_image_size( 'page-header-visual', 1710, 638, true );
	add_image_size( 'cursus-loop-thumbnail', 504, 315, true );
	add_image_size( 'groep-loop-thumbnail', 504, 432, true );
	
	/*
	Enable Ajax calls (register ajaxurl variable) 
	*/
	add_action('wp_head','bwh_set_global_ajaxurl');
	function bwh_set_global_ajaxurl() {
		echo "<script type='text/javascript'>var ajaxurl = '".admin_url('admin-ajax.php')."';</script>";
	}
	
	
	function add_grav_forms(){
	    $role = get_role('editor');
	    $role->add_cap('gform_full_access');
	}
	add_action('admin_init','add_grav_forms');

	add_action('wp_ajax_bwh_load_content_hook', 'bwh_load_content');
	add_action('wp_ajax_nopriv_bwh_load_content_hook', 'bwh_load_content');
	function bwh_load_content() {

		// Gather details
		$params=$url=$status=$content=NULL;
		$params = $_POST;
		$url = $params['permalink'];
		
		// Add site URL if missing
		if ( !strstr($params['permalink'],'http:',false) ) { $url = get_site_url().$url; }
		
		// Execute CURL command and fetch data
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($curl, CURLOPT_USERPWD, "bwh:ontwerpers");
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('X-Requested-With: XMLHttpRequest'));
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_POST, TRUE);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
		$content = curl_exec($curl);
		$status = curl_getinfo($curl);
		curl_close($curl);

		// Return content or status
		if ($content) { die($content); } 
		else { die(var_export($status, true)); }
	}
	
	/*
	Add columns to admin interface
	*/
	Jigsaw::add_column( 'docenten', 'Sleutels', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo '#'.get_field( 'id-docent' ).' ('.get_field('docent-code').')';
		echo '</a>';
	}, 2 );	
	Jigsaw::add_column( 'docenten', 'E-mailadres', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="mailto:'.get_field( 'email-adres' ).'" target="_blank">';
		echo get_field( 'email-adres' );
		echo '</a>';
	}, 3 );
	Jigsaw::add_column( 'docenten', 'Telefoonnummer', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="tel:'.get_field( 'telefoon' ).'" target="_blank">';
		echo get_field( 'telefoon' );
		echo '</a>';
	}, 4 );
	Jigsaw::add_column( 'docenten', 'Geboortedatum', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo get_field( 'geboorte-datum' );
		echo '</a>';
	}, 5 );

	Jigsaw::add_column( 'tarieven', 'Sleutels', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo '#'.get_field( 'id-tarief' ).' ('.get_field('tarief-code').')';
		echo '</a>';
	}, 2 );	
	Jigsaw::add_column( 'tarieven', 'Seizoen', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo get_field( 'seizoe-num' );
		echo '</a>';
	}, 3 );
	Jigsaw::add_column( 'tarieven', 'Tarief', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo '&euro; '.get_field( 'tar-bdrg' );
		echo '</a>';
	}, 4 );
	Jigsaw::add_column( 'tarieven', 'Tonen', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		if ( 1 == get_field( 'Op-website-tonen' ) ) {
			echo "Ja";
		} else {
			echo "Nee";
		}
		echo '</a>';
	}, 5 );
	Jigsaw::add_column( 'tarieven', 'HaFa', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		if ( 'hafa' == get_field( 'inkcat' ) ) {
			echo "Ja";
		} else {
			echo "Nee";
		}
		echo '</a>';
	}, 6 );
		
	Jigsaw::add_column( 'roosters', 'Seizoen', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo get_field( 'seizoe-num' );
		echo '</a>';
	}, 2 );
	Jigsaw::add_column( 'roosters', 'Datum', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo get_field( 'startdatum' ).' tot '.get_field( 'einddatum' );
		echo '</a>';
	}, 3 );
	Jigsaw::add_column( 'roosters', 'Dag', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo get_field( 'lesdag' );
		echo '</a>';
	}, 4 );
	Jigsaw::add_column( 'roosters', 'Tijden', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo get_field( 'aanvang' ).' tot '.get_field( 'eindtijd' );
		echo '</a>';
	}, 5 );
	
	Jigsaw::add_column( 'blokvormen', 'Seizoen', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo get_field( 'seizoe-num' );
		echo '</a>';
	}, 2 );
	Jigsaw::add_column( 'blokvormen', 'Datum', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo get_field( 'startdatum' ).' tot '.get_field( 'einddatum' );
		echo '</a>';
	}, 3 );
	Jigsaw::add_column( 'blokvormen', 'Cursus', function( $pid ) { // Add thumbnail to projecten
		$cursus = get_post( array( 'post_type' => 'cursussen', 'meta_key' => 'id-cursus', 'meta_value' => get_field( 'id-cursus' ), 'posts_per_page' =>1 ) );
		echo '<a href="/wp-admin/post.php?post='.$cursus[0]->ID.'&action=edit">';
		echo get_field( 'id-cursus' );
		echo '</a>';
	}, 4 );
	Jigsaw::add_column( 'blokvormen', 'Tijden', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo get_field( 'aanvang' ).' tot '.get_field( 'eindtijd' );
		echo '</a>';
	}, 5 );
	Jigsaw::add_column( 'blokvormen', 'Tonen op web', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		if ( 1 == get_field( 'Op_website_tonen' ) ) {
			echo "Ja";
		} else {
			echo "Nee";
		}
		echo '</a>';
	}, 6 );
	
	Jigsaw::add_column( 'gebouwen', 'Plaats', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo get_field( 'woonpl-naam' );
		echo '</a>';
	}, 2 );
	Jigsaw::add_column( 'gebouwen', 'Gemeente', function( $pid ) { // Add thumbnail to projecten
		echo '<a href="/wp-admin/post.php?post='.$pid.'&action=edit">';
		echo get_field( 'gemeen-naam' );
		echo '</a>';
	}, 3 );

	Jigsaw::add_column( 'cursussen', 'Relaties', function( $pid ) { // Add thumbnail to projecten
		if ( get_field( 'related-roosters' ) ) { 
			$roosters = get_field( 'related-roosters' );
			echo '<a href="/wp-admin/edit.php?s='.$pid.'&post_status=all&post_type=roosters">'; 
			echo "<i class='fa fa-calendar'></i> ".(empty($roosters) ? '0' : count($roosters))."&nbsp;&nbsp;&nbsp;";
			echo "</a>";
		}
		elseif ( get_field( 'related-blokvormen' ) ) { 
			$blokvormen = get_field( 'related-blokvormen' );
			echo '<a href="/wp-admin/edit.php?post_status=all&post_type=blokvormen">'; 
			echo "<i class='fa fa-calendar-o'></i> ".(empty($blokvormen) ? '0' : count($blokvormen))."&nbsp;&nbsp;&nbsp;"; 
			echo "</a>";
		}
		else { echo "<i class='fa fa-calendar-o'></i> 0&nbsp;&nbsp;&nbsp;"; }
		$lokalen = get_field( 'related-lokalen' ); echo " <i class='fa fa-map-marker'></i> ".(empty($lokalen) ? '0' : count($lokalen))."&nbsp;&nbsp;&nbsp;";	
		$docenten = get_field( 'related-docenten' ); echo " <i class='fa fa-users'></i> ".(empty($docenten) ? '0' : count($docenten))."&nbsp;&nbsp;&nbsp;";
		$tarieven = get_field( 'related-tarieven' ); echo " <i class='fa fa-money'></i> ".(empty($tarieven) ? '0' : count($tarieven));
		echo '</a>';
	}, 3 );

	require_once(PHPFOLDER.'/toanhus.tarieven.calculator.php');
	
	/*
	Check if announcements should be displayed (based on session)
	*/
	function bwh_show_announcements() {
		return ! ( isset($_SESSION['announcement_closed']) && ('1' == $_SESSION['announcement_closed']) );
	}
	
	/*
	Function to set session that indicates visibility of announcements
	*/
	add_action('wp_ajax_bwh_hide_announcements_hook', 'bwh_hide_announcements');
	add_action('wp_ajax_nopriv_bwh_hide_announcements_hook', 'bwh_hide_announcements');
	function bwh_hide_announcements() {
		$wp_session = WP_Session::get_instance();
		$wp_session['announcement_closed'] = 1;
		echo 1;
		die();
	}
	
	/*
	Get number of results based on form
	*/
	add_action('wp_ajax_bwh_cursus_finder_calculate_hook', 'bwh_cursus_finder_calculate');
	add_action('wp_ajax_nopriv_bwh_cursus_finder_calculate_hook', 'bwh_cursus_finder_calculate');
	function bwh_cursus_finder_calculate() {
		
		$locaties = $_POST['locaties'];
		$leeftijden = $_POST['leeftijden'];
		$groepen = $_POST['groepen'];
		
		$tax_query = array();
		$tax_terms = array();
		$json_terms = array();
		
		$tax_terms['locaties'] = array();
		$tax_terms['leeftijden'] = array();
		$tax_terms['groepen'] = array();
		
		if (count($locaties)>0) { array_push($tax_query, array( 'taxonomy' => 'locaties', 'field' => 'term_id', 'terms' => $locaties, ) ); }
		if (count($leeftijden)>0) { array_push($tax_query, array( 'taxonomy' => 'leeftijden', 'field' => 'term_id', 'terms' => $leeftijden, ) ); }
		if (count($groepen)>0) { array_push($tax_query, array( 'taxonomy' => 'groepen', 'field' => 'term_id', 'terms' => $groepen, ) ); }
		
		if ( $locaties || $leeftijden || $groepen ) {
		
			$aanbodQuery = new WP_Query( array( 'fields' => 'ids', 'no_found_rows' => true, 'update_post_meta_cache' => false, 'post_type' => 'cursussen', 'posts_per_page' => -1, 'tax_query' => $tax_query ) );
			
			while ( $aanbodQuery->have_posts() ) {
				$aanbodQuery->the_post();
	
				$post_terms = wp_get_post_terms(get_the_ID(), array('locaties') );
				foreach ( $post_terms as $post_term ) { array_push( $tax_terms['locaties'], $post_term->term_id ); }
	
				$post_terms = wp_get_post_terms(get_the_ID(), array('leeftijden') );
				foreach ( $post_terms as $post_term ) { array_push( $tax_terms['leeftijden'], $post_term->term_id ); }
				
				$post_terms = wp_get_post_terms(get_the_ID(), array('groepen') );
				foreach ( $post_terms as $post_term ) { array_push( $tax_terms['groepen'], $post_term->term_id ); }
			}
			
			$tax_terms['count'] = $aanbodQuery->post_count;
			$tax_terms['locaties'] = array_unique( $tax_terms['locaties'] );
			$tax_terms['leeftijden'] = array_unique( $tax_terms['leeftijden'] );
			$tax_terms['groepen'] = array_unique( $tax_terms['groepen'] );
			
		} else {
			
			$tax_terms['count'] = -1;
			
		}
				
		echo json_encode($tax_terms);
				
		die();
	}
	
	/*
	Site colors
	*/
	global $colors;
	$colors = array('#522583','#86d0f5','#e84552','#ffed00');
	
	/*
	Get 'from' number 'lesduur' for cursus based on rosters
	*/
	function bwh_get_min_lesduur($cursus) {
		
		global $tijden;
		
		$rosters = get_field( 'related-roosters', $cursus );
		if ( !get_field( 'related-roosters', $cursus ) ) {
			$rosters = get_field( 'related-blokvormen', $cursus );
		}
		$times = array();
		
		foreach ( $rosters as $roster ) {
			array_push( $times, get_field( 'lestijd', $roster ) );
		}
		
		asort($times);
		
		$time_min = array_shift($times);
		
		if ( count($times) > 0 ) {
			$time_max = array_pop($times);
		} else {
			$time_max = $time_min;
		}
		
		if ( $time_min != $time_max ) {
			return $tijden[$time_min] . " - " . $tijden[$time_max];
		} elseif ( !$tijden[$time_min] ) {
			return false;
		} else {
			return $tijden[$time_min];
		}
		
	}
	
	/*
	Get 'from' number of 'lessen' for cursus based on rosters
	*/
	function bwh_get_min_lessen( $cursus, $range ) {
		
		$rosters = get_field( 'related-roosters', $cursus );
		if ( !get_field( 'related-roosters', $cursus ) ) {
			$rosters = get_field( 'related-blokvormen', $cursus );
		}
		$lessen = array();
		
		foreach ( $rosters as $roster ) {
			array_push( $lessen, get_field( 'lessen', $roster ) );
		}
		
		asort($lessen);
		
		$aantal_min = array_shift($lessen);
		
		if ( count($lessen) > 0 ) { 
			$aantal_max = array_pop($lessen);
		} else {
			$aantal_max = $aantal_min;
		}
		
		if ( true == $range ) {

			if ( $aantal_min != $aantal_max ) {
				return $aantal_min . " - " . $aantal_max;
			} else {
				return $aantal_min;
			}

		} else {
			
			return $aantal_min;
			
		}

	}
	
/*
	Get 'to' number of 'lessen' for cursus based on rosters
	*/
	function bwh_get_max_lessen( $cursus, $range ) {
				
		$rosters = get_field( 'related-roosters', $cursus );
		if ( !get_field( 'related-roosters', $cursus ) ) {
			$rosters = get_field( 'related-blokvormen', $cursus );
		}
		$lessen = array();
		
		foreach ( $rosters as $roster ) {
			array_push( $lessen, get_field( 'lessen', $roster ) );
		}
		
		asort($lessen);
		
		$aantal_max = array_pop($lessen);
			
		return $aantal_max;
			
	}
	
	/*
	Get lesdagen for cursus
	*/
	function sortArrayByArray(array $toSort, array $sortByValuesAsKeys)
	{
	    $commonKeysInOrder = array_intersect_key(array_flip($sortByValuesAsKeys), $toSort);
	    $commonKeysWithValue = array_intersect_key($toSort, $commonKeysInOrder);
	    $sorted = array_merge($commonKeysInOrder, $commonKeysWithValue);
	    return $sorted;
	}

	function bwh_get_lesdagen( $cursus ) {
		
		global $weekdagen;
		
		$rosters = get_field( 'related-roosters', $cursus );
		if ( !get_field( 'related-roosters', $cursus ) ) {
			$rosters = get_field( 'related-blokvormen', $cursus );
		}
		$dagen = array();
		
		foreach ( $rosters as $roster ) {
			$dagen[get_field( 'lesdag', $roster )] = get_field( 'lesdag', $roster );
		}
		
		$print_dagen = '';
		$dagen = array_unique( $dagen );
		$dagen_sorted = array_merge( array_flip( array('Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag', 'Zondag') ), $dagen );
		foreach ( $dagen_sorted as $key => $dag ) {
			if ( !is_numeric($dag) ) {
				$print_dagen .= strtolower( $dag ).", "; 
			}
		}

		return substr( $print_dagen, 0, -2 );
		
	}
	
	/*
	Get startdatum for cursus
	*/
	function bwh_get_startdatum( $cursus ) {
		
		if ( get_field( 'startdatum', $cursus ) ) {
			
			return "vanaf " . date( 'd/m/Y', strtotime( bwh_convert_date( get_field( 'startdatum', $cursus ) ) ) );
			
		} else {
			
			return false;
			
		}
		
	}
	
	/*
	Modify video embeds
	*/
	add_filter('video_embed_html', 'bwh_embed_oembed_html', 99, 4);
	add_filter('embed_oembed_html', 'bwh_embed_oembed_html', 99, 4);
	function bwh_embed_oembed_html($html, $url, $attr, $post_id) {
	  return '<div class="video__wrapper">' . $html . '</div>';
	}
	
	/*
	Mask future posts as published posts
	*/
	remove_action( 'future_post', '_future_post_hook', 5, 2 );
	add_action( 'future_post', 'bwh_future_post_hook', 5, 2);
	function bwh_future_post_hook( $deprecated = '', $post ) {
    	wp_publish_post( $post->ID );
	}
	
	/*
	Modify GF next button for form
	*/

	add_filter( 'gform_next_button_1', 'bwh_custom_next_button', 10, 2 );
	function bwh_custom_next_button( $next_button, $form ) {
		$next_button = '<div class="block__cursus__inschrijven gform__next__button--next"><i class="fa fa-fw fa-arrow-right ani__all"></i>' . $next_button . '</div>';
		return $next_button;
	}

	add_filter( 'gform_previous_button_1', 'bwh_custom_previous_button', 10, 2 );
	function bwh_custom_previous_button( $next_button, $form ) {
		$next_button = '<div class="block__cursus__inschrijven gform__next__button--next gform__next__button--prev"><i class="fa fa-fw fa-arrow-left ani__all"></i>' . $next_button . '</div>';
		// $next_button .= '<button class="block__cursus__inschrijven gform__next__button--next gform__next__button--prev"><i class="fa fa-fw fa-arrow-left ani__all"></i><span class="ani__all">Vorige stap 1/3</span></button>';
		return $next_button;
	}
	add_filter( 'gform_submit_button_4', 'bwh_roosters_next_button', 10, 2 );
	function bwh_roosters_next_button( $next_button, $form ) {
		$next_button = '<div class="block__cursus__inschrijven gform__next__button--next"><i class="fa fa-fw fa-arrow-right ani__all"></i>' . $next_button . '</div>';
		// $next_button .= '<button class="block__cursus__inschrijven gform__next__button--next"><i class="fa fa-fw fa-arrow-right ani__all"></i><span class="ani__all">Volgende stap 1/3</span></button>';
		return $next_button;
	}
	
	add_filter( 'gform_submit_button_1', 'bwh_roosters_submit_button', 10, 2 );
	function bwh_roosters_submit_button( $next_button, $form ) {
		$next_button = '<div class="block__cursus__inschrijven gform__next__button--finish"><i class="fa fa-fw fa-arrow-right ani__all"></i>' . $next_button . '</div>';
		// $next_button .= '<button class="block__cursus__inschrijven gform__next__button--finish"><i class="fa fa-fw fa-arrow-right ani__all"></i><span class="ani__all">Inschrijving voltooien</span></button>';
		return $next_button;
	}
	
	// Set the startdatum into a hidden vield	
/*	add_filter( 'gform_field_value_startdate', 'populate_startdate' );
	function populate_startdate( $value ) {
	   return date('d/m/Y', strtotime( bwh_convert_date( get_field( 'startdatum', $roster ) ) ) );
	}
*/	
	/*
	Modify 'inschrijf formulier' error message
	*/
	add_filter( 'gform_validation_message_1', 'bwh_change_error_message', 10, 2 );
	function bwh_change_error_message( $message, $form ) {
	    return "<div class='validation_error'>Nog niet alle verplichte velden zijn ingevuld</div>";
	}

	
	function bwh_output_cursus_thankyou() {
	
		return esc_html( $_GET['cursus'] );
			
	}
	add_shortcode( 'parsed-cursus', 'bwh_output_cursus_thankyou' );
	
	function bwh_cursus_title($title) {
		$elements = explode( "|", $title );
		if ( count($elements) > 1 ) {
			return $elements[0]."<span>".$elements[1]."</span>";
		} else {
			return $elements[0];
		}
		//return str_replace( "|", "<br/>", $title );
	}
	
	add_action( 'pre_get_posts', 'bwh_modify_aanbod_sort' );
	function bwh_modify_aanbod_sort($query) {
		
		if ( $query->is_tax('groepen') && $query->is_main_query() ) {
			$query->set( 'orderby', 'title' );
			$query->set( 'order', 'ASC' );
			return;
		}
		
		if ( $query->is_category( 'nieuws' ) && $query->is_main_query() ) {
			$query->set( 'offset', 1 );
			return;
		}
		
		if ( $query->is_category( 'agenda' ) && $query->is_main_query() ) {
			$query->set( 'orderby', 'date' );
			$query->set( 'order', 'ASC' );
			$query->set( 'no_found_rows', true );
			$query->set( 'update_post_term_cache', false );
			$query->set( 'update_post_meta_cache', false );
			$query->set( 'date_query', array( array( 'after' => 'yesterday' ) ) );
			return;
		}
		
	}
	
	/*
	Modify default excerpt length
	*/
	add_filter( 'excerpt_length', 'bwh_custom_excerpt_length', 999 );
	add_filter( 'excerpt_more', 'bwh_excerpt_more' );
	function bwh_custom_excerpt_length( $length ) { return 20; }
	function bwh_excerpt_more( $more ) { return '&hellip;'; }
	
	/*
	Convert NL date to EN date
	*/
	function bwh_convert_date($date) {
		$elements = explode( "/", $date ); // d / m / y
		if ( 2 == strlen( $elements[2] ) ) { $elements[2] = '20'.$elements[2]; }
		return $elements[2]."/".$elements[1]."/".$elements[0];
	}
	
	
	//add_filter( 'gform_progressbar_start_at_zero', '__return_true' );
	
function my_acf_google_map_api( $api ){

  $api['key'] = 'AIzaSyAjDm6273wigQ7Jgx0HVZ6NEcdHGAu503U';

  return $api;

}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
	

?>