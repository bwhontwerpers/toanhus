		
		<div class="footer__bumper">&nbsp;</div>
		
		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="footer__wrapper">
						<div class="col-md-5">
							<?php wp_nav_menu( array('theme_location' => 'topleft', 'container' => false, 'menu_class' => 'header__menu header__menu--left' ) ); ?>
						</div>
						<div class="col-md-6">
							<!-- <button class="header__search__button"><i class="fa fa-fw fa-search"></i></button> -->
							<?php wp_nav_menu( array('theme_location' => 'bottomright', 'container' => false, 'menu_class' => 'header__menu header__menu--right' ) ); ?>
						</div>
						<div class="col-md-1">
							<div class="social__menu header__menu header__menu--right">
								<a href="https://www.instagram.com/toanhus_insta/"><i class="fa fa-instagram"></i></a>&nbsp;
								<a href="https://www.facebook.com/Toanhus"><i class="fa fa-facebook"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	
	
	
	<div class="device_desktop"></div>
	<div class="device_tablet"></div>
	<div class="device_smartphone"></div>
	
	<?php wp_footer(); ?>

</body>
</html>