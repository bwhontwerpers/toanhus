<?php 
/* Category - nieuws */ 
get_header();
?>


<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__header__visual__wrapper">
					<button class="container__header__back" onclick="window.history.go(-1);"><i class="ani__all fa fa-chevron-left fa-fw"></i></button>
					<?php 
					if ( get_field( 'groep-header', 'groepen_'.get_queried_object()->term_id ) ) { $header = get_field( 'groep-header', 'groepen_'.get_queried_object()->term_id ); } 
					else { $header = get_field( 'global-aanbod-header-visual','option' ); }
					echo wp_get_attachment_image($header , 'page-header-visual', false, array( 'class' => 'container__header__visual' ) );
					?>
				</div>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>