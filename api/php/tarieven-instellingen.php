<?php
add_action('acf/init', 'add_tarieven_instellingen');

function add_tarieven_instellingen() {

      // Check function exists. 
  if( function_exists('acf_add_options_page') ) {

    acf_add_options_page( [
      'page_title'    => 'Tarieven instellingen',
      'menu_title'    => 'Instellingen',
      'menu_slug'     => 'tarieven-settings',
      'parent_slug'   => '/edit.php?post_type=tarieven',
      'capability'    => 'edit_posts',
      'redirect'      => false
    ]);

  }
}
?>