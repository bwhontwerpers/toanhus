<?php
/*
Toanhus mappings
*/
function toanhus_get_mappings($type) {
	
	if ( 'lestypen' == $type ) {
		/*
		return array(
				'I40' => 'Individueel 40 minuten',
				'G60' => 'Groepsles 60 minuten',
				'S30' => 'Knipkaart 30 minuten',
				'S20' => 'Knipkaart 20 minuten',
				'I30' => 'Individueel 30 minuten',
				'I15' => 'Individueel 15 minuten',
				'G30' => 'Groepsles 30 minuten',
				'I20' => 'Individueel 20 minuten',
				'G15' => 'Groepsles 15 minuten',
			);
		*/
			
		return array(
				'5' => 'Mixed Music',
				'4' => 'Knipkaart',
				'3' => 'Groepsles',
				'2' => 'Individueel',
		);
		
	}
	
	if ( 'frequenties' == $type ) {
		
		return array(
				'1wk' => '1x per week',
				'2wk' => '1x per twee weken',
				'3wk' => '1x per drie weken',
				'4wk' => '1x per maand',
			);
	}
	
}

/*
Toanhus tarieven calculator functions
*/

add_filter( 'gform_admin_pre_render_2', 'toanhus_populate_gf_calculator_fields' );
add_filter( 'gform_pre_render_2', 'toanhus_populate_gf_calculator_fields' );
add_filter( 'gform_pre_validation_2', 'toanhus_populate_gf_calculator_fields' );
add_filter( 'gform_pre_submission_filter_2', 'toanhus_populate_gf_calculator_fields' );
add_filter( 'gform_admin_pre_render_4', 'toanhus_populate_gf_calculator_fields' );
add_filter( 'gform_pre_render_4', 'toanhus_populate_gf_calculator_fields' );
add_filter( 'gform_pre_validation_4', 'toanhus_populate_gf_calculator_fields' );
add_filter( 'gform_pre_submission_filter_4', 'toanhus_populate_gf_calculator_fields' );
function toanhus_populate_gf_calculator_fields( $form ) {

	global $post;
	
	//Get current cursus (post)
	//$cursusid = 946; // Met hafa
	//$cursusid = 934; // Zonder hafa
	//$cursusid = $_GET['dynamic_cursusid'];
	//if (!$cursusid) { $cursusid = 960; }
	
	$cursus = get_post($id=$post->ID);

	// FALLBACK
	
	
	/*
	MANUAL MAPPINGS
	*/
	$lestypen_mapping = toanhus_get_mappings('lestypen');	
	$frequenties_mapping = toanhus_get_mappings('frequenties');

	/*
	FETCH TARIEVEN
	*/
	if ( isset($cursus->ID) ) {
				
		$tarieven = get_field( 'related-tarieven', $cursus->ID );
				
		$tarievenQuery = new WP_query( array( 'cache_results' => false, 'update_post_term_cache' => false, 'post_type'=>'tarieven', 'posts_per_page'=>-1, 'post__in' => $tarieven ) );
		
		foreach ( $form['fields'] as $key => &$field ) {
			
			/*
			POPULATE MUZIEKVERENIGINGEN DROPDOWN
			*/			
			if ( $field->id == 6 ) {
								
				$items = array();
				$hafaenabled = false;
				
				while ( $tarievenQuery->have_posts() ) {
					$tarievenQuery->the_post();
	
					if ('hafa' == get_field( 'inkcat' ) ) {
						$hafaenabled = true;
					}
				
				} wp_reset_query();
	
				if ( true == $hafaenabled ) {
				
					//$items[] = array( 'value' => '', 'text' => 'Nee' );
					
					$verenigingenQuery = new WP_query( array( 'cache_results' => false, 'update_post_term_cache' => false, 'post_type'=>'verenigingen', 'posts_per_page'=>-1 ) );
					while ( $verenigingenQuery->have_posts() ) {
						$verenigingenQuery->the_post();
						$items[] = array( 'value' => get_the_ID(), 'text' => get_the_title() );
					} wp_reset_query();
					
					asort($items);
					
					$field->choices = $items;
					
				} else {
					
					unset($form['fields'][$key]);
						
				}		
				
			}
		
			/*
			POPULATE FREQUENTIE DROPDOWN
			*/
		    if ( $field->id == 5 ) {
				$items = array();
				$frequenties = array();	
				while ( $tarievenQuery->have_posts() ) {
					$tarievenQuery->the_post();
					array_push( $frequenties, get_field( 'freq' ) );	
				}
				wp_reset_query();
				$frequenties = array_unique($frequenties);
				foreach ( $frequenties as $frequentie ) {
					if ( isset( $frequenties_mapping[$frequentie] ) ) {
					    $items[] = array( 'value' => $frequentie, 'text' => $frequenties_mapping[$frequentie] );
					}
				}
				
				asort($items);
		        
		        $field->choices = $items;
		    }
		    
			/*
			POPULATE LEEFTIJDEN
			*/
			if ( $field->id == 1 ) {
				$items = array();
				$leeftijden = array();	
				while ( $tarievenQuery->have_posts() ) {
					$tarievenQuery->the_post();
					array_push( $leeftijden, get_field( 'lftcat-oms' ) );	
				}
				wp_reset_query();
				$leeftijden = array_unique($leeftijden);
				foreach ( $leeftijden as $leeftijd ) {
				    $items[] = array( 'value' => $leeftijd, 'text' => $leeftijd );
				}
		        	        
		        $field->choices = $items;
		    }
	
			/*
			POPULATE MINUTEN
			*/
			if ( $field->id == 4 ) {
				$items = array();
				$minuten = array();	
				while ( $tarievenQuery->have_posts() ) {
					$tarievenQuery->the_post();
					array_push( $minuten, get_field( 'lestijd' ) );	
				}
				wp_reset_query();
				$minuten = array_unique($minuten);
				foreach ( $minuten as $minuut ) {
				    $items[] = array( 'value' => $minuut, 'text' => $minuut.' minuten' );
				}
				
				asort($items);
		            
		        $field->choices = $items;
		    }
	
			/*
			POPULATE LESTYPE
			*/
			if ( $field->id == 2 ) {
				$items = array();
				$typen = array();	
				while ( $tarievenQuery->have_posts() ) {
					$tarievenQuery->the_post();
					array_push( $typen, get_field( 'lessoort' ) );	
				}
				wp_reset_query();
				$typen = array_unique($typen);
				foreach ( $typen as $type ) {
					if ( isset( $lestypen_mapping[$type] ) ) { 
						$items[] = array( 'value' => $type, 'text' => $lestypen_mapping[$type] ); 
					}
				}
		        
		        asort($items);
		        
		        $field->choices = $items;
		    }
	
		    
		}

	}
	
	return $form;
	
}

add_action('wp_ajax_toanhus_update_gf_calculator_fields_hook', 'toanhus_update_gf_calculator_fields');
add_action('wp_ajax_nopriv_toanhus_update_gf_calculator_fields_hook', 'toanhus_update_gf_calculator_fields');
function toanhus_update_gf_calculator_fields() {
		
	write_log("");
	write_log("toanhus_update_gf_calculator_fields");
	write_log("------------------------------------");
		
	$filter_leeftijd = $_POST['leeftijd'];
	$filter_lesvorm = $_POST['lesvorm'];
	$filter_minuten = $_POST['minuten'];
	$filter_frequentie = $_POST['frequentie'];
	$filter_cursus = $_POST['cursus'];
	$filter_hafa = $_POST['hafa'];
	
	$return_leeftijden = array();
	$return_lesvormen = array();
	$return_minuten = array();
	$return_frequenties = array();
	$return_rosters = array();
	
	$store_leeftijden = array();
	$store_lesvormen = array();
	$store_minuten = array();
	$store_frequenties = array();
	$store_aantallessen = array();
	$store_rosters = array();
	$store_tarieven = array();

	/*
	GET CURSUS
	*/
	$cursus = get_post($id=$filter_cursus);
	
	write_log("Getting tarieven for {$filter_cursus}");
	
	/*
	GET MAPPINGS
	*/
	$lestypen_mapping = toanhus_get_mappings('lestypen');	
	$frequenties_mapping = toanhus_get_mappings('frequenties');

	/*
	BUILD META QUERY
	*/
	
	$meta_query = array();
	
	if ( $filter_leeftijd ) {
		array_push($meta_query, 
			array(
			'key' => 'lftcat-oms',
			'value' => $filter_leeftijd,
			'compare' => '=',
			)
		);
	}
	
	if ( $filter_lesvorm ) {
		array_push($meta_query, 
			array(
			'key' => 'lessoort',
			'value' => $filter_lesvorm,
			'compare' => '=',
			)
		);
	}
	
	if ( $filter_minuten ) {
		array_push($meta_query, 
			array(
			'key' => 'lestijd',
			'value' => $filter_minuten,
			'compare' => '=',
			)
		);
	}
	
	if ( $filter_frequentie ) {
		array_push($meta_query, 
			array(
			'key' => 'freq',
			'value' => $filter_frequentie,
			'compare' => '=',
			)
		);
	}

	if ( $filter_hafa ) {
		array_push($meta_query, 
			array(
			'key' => 'inkcat',
			'value' => 'hafa',
			'compare' => '=',
			)
		);		
		write_log("Setting HAFA filter to TRUE");
	} else {
		array_push($meta_query, 
			array(
			'key' => 'inkcat',
			'value' => 'hafa',
			'compare' => '!=',
			)
		);				
		write_log("Setting HAFA filter to FALSE");
	}

	array_push($meta_query, 
		array(
		'key' => 'Op-website-tonen',
		'value' => '1',
		'compare' => '=',
		)
	);
			
	/*
	FETCH TARIEVEN BASED ON FILTERS
	*/
	$tarieven = get_field( 'related-tarieven', $filter_cursus ); // Fetch all tarieven that are linked to cursus
	
	$tarievenQuery = new WP_query( 
		array( 'post_type'=>'tarieven',
			   'posts_per_page'=>-1,
			   'post__in' => $tarieven,
			   'meta_query' => array(
				   'relation' => 'AND',
				   $meta_query,				   
			   )
			   
		) 
	);

	$les_tarief_btw = 0;
	while ( $tarievenQuery->have_posts() ) {
		
		$tarievenQuery->the_post();
		array_push( $store_leeftijden, get_field( 'lftcat-oms' ) );
		array_push( $store_lesvormen, get_field( 'lessoort' ) );
		array_push( $store_minuten, get_field( 'lestijd' ) );
		array_push( $store_frequenties, get_field( 'freq' ) );
		array_push( $store_tarieven, get_the_ID() );
		
		array_unique($store_leeftijden);
		array_unique($store_lesvormen);
		array_unique($store_minuten);
		array_unique($store_frequenties);
			
		$les_tarief = get_field( 'tar-bdrg' );
		$les_aantallessen = get_field( 'aantal-lessen-tarief' );
		$tarief_title = get_the_title();
		if ( strstr($tarief_title,'> 21') || strstr($tarief_title,'&gt; 21') ) {
			$les_tarief_btw = 1;
		}
	}
	
	
	wp_reset_query();
	
	if ( has_term( 'instrumenten', 'groepen', $filter_cursus ) ) {
		$store_rosters = get_field( 'related-blokvormen', $filter_cursus );
	} else {
		$store_rosters = get_field( 'related-roosters', $filter_cursus );
	}
		
	if ($store_rosters) {
		foreach ( $store_rosters as $store_roster ) {
			if ( 1 == get_field( 'Op_website_tonen', $store_roster ) ) {
				$roster_object = get_post( $id = $store_roster );
				$locaties = get_terms( array('locaties'), array( 'hide_empty' => 0 ) );
				foreach ( $locaties as $locatie ) {
					if ( get_field( 'id-lokaal', 'locaties_'.$locatie->term_id ) ==  get_field( 'id-lokaal', $store_roster ) ) {
						$return_rosters[] = array( 'value' => $store_roster, 'text' => get_field( 'woonpl-naam', 'locaties_'.$locatie->term_id ) );
					}
				}
				write_log("Rooster {$store_roster} gevonden" );
			}
		}
	}
		
	$store_leeftijden = array_unique( $store_leeftijden );
	foreach ( $store_leeftijden as $store_leeftijd ) {
		if ( $filter_leeftijd == $store_leeftijd ) { $selected = 'selected'; } else { $selected = false; }
		$return_leeftijden[] = array( 'value' => $store_leeftijd, 'text' => $store_leeftijd, 'selected' => $selected );
	}
	
	$store_lesvormen = array_unique( $store_lesvormen );
	asort($store_lesvormen);
	foreach ( $store_lesvormen as $store_lesvorm ) {
		if ( $filter_lesvorm == $store_lesvorm ) { $selected = 'selected'; } else { $selected = false; }
		if ( isset( $lestypen_mapping[$store_lesvorm] ) ) {
			$return_lesvormen[] = array( 'value' => $store_lesvorm, 'text' => $lestypen_mapping[$store_lesvorm], 'selected' => $selected );
		}
	}
	
	$store_minuten = array_unique( $store_minuten );
	foreach ( $store_minuten as $store_minuut ) {
		if ( $filter_minuten == $store_minuut ) { $selected = 'selected'; } else { $selected = false; }
		$return_minuten[] = array( 'value' => $store_minuut, 'text' => $store_minuut.' minuten', 'selected' => $selected );
	}
	
	$store_frequenties = array_unique( $store_frequenties );
	foreach ( $store_frequenties as $store_frequentie ) {
		if ( $filter_frequentie == $store_frequentie ) { $selected = 'selected'; } else { $selected = false; }
		if ( isset( $frequenties_mapping[$store_frequentie] ) ) {
			$return_frequenties[] = array( 'value' => $store_frequentie, 'text' => $frequenties_mapping[$store_frequentie], 'selected' => $selected );
		}
	}

	$store_aantallessen = array_unique( $store_aantallessen );
	foreach ( $store_aantallessen as $store_aantalles ) {
		$return_minuten[] = array( 'value' => $store_minuut, 'text' => $store_minuut.' minuten', 'selected' => $selected );
	}	
	
	write_log("Tarieven calculator heeft ".$tarievenQuery->post_count." tarieven gevonden");
	write_log(print_r($store_tarieven,true));
	write_log(print_r($store_aantallessen,true));

		
	if ( 1 == $tarievenQuery->post_count && has_term( 'instrumenten', 'groepen', $filter_cursus ) ) {
		echo json_encode( 
			array('roosters'=>$return_rosters,
				  'leeftijden'=>$return_leeftijden,
				  'lesvorm'=>$return_lesvormen,
				  'minuten'=>$return_minuten,
				  'frequentie'=>$return_frequenties,
				  'price'=>$les_tarief,
				  'btw'=>$les_tarief_btw,
				  'aantallessen'=>$les_aantallessen
				 )
			);	

	} elseif ( get_field( 'tar-bdrg', $filter_cursus ) && !has_term( 'instrumenten', 'groepen', $filter_cursus ) ) {
		echo json_encode( 
			array('roosters'=>$return_rosters,
				  'leeftijden'=>$return_leeftijden,
				  'lesvorm'=>$return_lesvormen,
				  'minuten'=>$return_minuten,
				  'frequentie'=>$return_frequenties,
				  'price'=>get_field( 'tar-bdrg', $filter_cursus ),
				  'btw'=>$les_tarief_btw,
				  'aantallessen'=>$les_aantallessen
				 ) 
			);	
	
	} elseif (2 == $tarievenQuery->post_count && has_term( 'instrumenten', 'groepen', $filter_cursus ) && !$filter_hafa ) {
		$return_rosters = array_shift($return_rosters);
		echo json_encode( 
			array('roosters'=>$return_rosters,
				  'leeftijden'=>$return_leeftijden,
				  'lesvorm'=>$return_lesvormen,
				  'minuten'=>$return_minuten,
				  'frequentie'=>$return_frequenties,
				  'price'=>$les_tarief,
				  'btw'=>$les_tarief_btw,
				  'aantallessen'=>$les_aantallessen
				) 
			);
			
	} else {
		echo json_encode( 
			array('roosters'=>$return_rosters,
				  'leeftijden'=>$return_leeftijden,
				  'lesvorm'=>$return_lesvormen,
				  'minuten'=>$return_minuten,
				  'frequentie'=>$return_frequenties,
				  'price'=>0,
				  'btw'=>$les_tarief_btw,
				  'aantallessen'=>$les_aantallessen
				 )
			);	
	}
	
	
	die();
}

add_action('wp_ajax_toanhus_finish_gf_calculator_hook', 'toanhus_finish_gf_calculator');
add_action('wp_ajax_nopriv_toanhus_finish_gf_calculator_hook', 'toanhus_finish_gf_calculator');
function toanhus_finish_gf_calculator() {
	
	
}

?>