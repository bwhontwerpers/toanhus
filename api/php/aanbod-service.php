<?php
// Creates singleton class for a service and adds its to a global var
class AanbodService {
  // singleton
  private static $instance = null;
  private function __construct(){}
  public static function i() {
    if (self::$instance == null) self::$instance = new AanbodService();
    return self::$instance;
  }
  // <--

  // Helper function for filterchecking
  public function hasFilter($filter) {
    if( isset($_POST[$filter]) && (count($_POST[$filter]) > 0) ) return true; return false;
  }
  public function getFilter($filter) {
    if( isset($_POST[$filter]) ) return $_POST[$filter]; return false;
  }

  private $cache = object; //new stdClass();

  // Caching the main-queryobject
  public function setCache( $term_id ) {
    if( isset($this->cache->id) ) return $this->cache;
    $this->cache = new stdClass();
    $this->cache->id = $term_id;
    $this->cache->term = get_term_by('id', $term_id, 'groepen' );
    $this->cache->children = get_terms( 'groepen', array( 'hide_empty' => 1, 'update_term_meta_cache' => false, 'parent' => $term_id ) );
    $this->cache->is_parent =  empty($this->cache->term->parent);
    $this->cache->image = $this->getImage($term_id );
    $this->cache->intro = get_field( 'groep-intro', 'groepen_'.$term_id); 
    return $this->cache;
  }

  public function getTerm( $term_id ) { // use: parent
    return get_term_by('id', $term_id, 'groepen' );
  }

  public function getImage( $term_id ) {
    $header = get_field( 'groep-header', 'groepen_'.$term_id); // groep has image
    if( empty($header) ) $header = get_field( 'global-aanbod-header-visual','option' ); // get fallback image from settings
    return wp_get_attachment_image_src($header , 'page-header-visual', false, array( 'class' => 'container__header__visual' ) );
  }

  public function getCourseImage( $post_id ) {
    $image = get_the_post_thumbnail( $post_id, 'cursus-loop-thumbnail', array( 'class' => 'loop__cursus__thumbnail ani__all' ) ); 
    if( !empty($image) ) return $image;
    $image = get_field( 'global-aanbod-default-poster', 'option' ); // get fallback image from settings
    return wp_get_attachment_image($image , 'cursus-loop-thumbnail', false, array( 'class' => 'loop__cursus__thumbnail ani__all' ) );
  }

  // private function terms_merge( $leadArray, $addArray ) {
  //   $existingKeys = [];
  //   foreach( $leadArray as $leadItem) $existingKeys[] = $leadItem->term_id;
  //   foreach( $addArray as $addItem) {
  //     if( !empty($addItem->parent) ) continue;
  //     if( array_key_exists( $addItem->term_id, $existingKeys) ) continue;
  //     array_push( $leadArray, $addItem);
  //   }
  //   return $leadArray;
  // }

  private function extract_location_terms( $terms ) {
    if( empty($terms) ) return [];
    $parentNames = [];
    $parentIds = [];
    foreach($terms as $term) {
      if( empty($term->parent) ) {
        $parentIds[] = $term->term_id; // parent is directly added
        $parentNames[] = $term->name;
      } else {
        $parentIds[] = $term->parent;
        $parentNames[] = get_term_by('id', $term->parent, 'locaties')->name;
      }
    }
    return [
      'ids' => array_unique($parentIds),
      'names' => array_unique($parentNames)
    ];
  }

  private function extract_ages_terms( $terms ) {
    if( empty($terms) ) return [];
    $ids = [];
    $names = [];
    foreach($terms as $term) {
      $ids[] = $term->term_id;
      $names[] = $term->name;
    }
    return  [
      'ids' => array_unique($ids),
      'names' => array_unique($names)
    ];
  }

  // Aanbod voor proeflessen > cursuslijst
  public function getAanbodCourses( $groep_id ) {
    // fetch parent and children groepid
    $args = [
      'post_type' => 'cursussen', // base query, paged,
      'posts_per_page' => -1,
      'post_status' => ['publish'],
      'orderby' => 'post_title',
      'order' => 'ASC'
    ];
    $tax_queries = ['relation' => 'AND'];
    $tax_queries[] = [
      'taxonomy' => 'groepen',
      'field' => 'term_id',
      'terms' => $groep_id,
      // 'operator' => '='
    ];
    $args['tax_query'] = $tax_queries;

    $query = new WP_Query($args);
    if( ! ( isset($query) && $query->have_posts() ) ) return false;
    $results = [];
    foreach( $query->posts as $post) {
      $results[] = [
        'id' => $post->post_name,
        'title' => $post->post_title
      ];
    }
    return $results;
  }

  // Aanbod voor overzicht met filters
  public function getAanbod( $groep_id = false ) { //, $locations_id = false, $age_id = false
    $args = [
      'post_type' => 'cursussen', // base query, paged,
      'posts_per_page' => -1,
      'post_status' => ['publish'],
      'orderby' => 'post_title',
      'order' => 'ASC'
    ];

    if( false != $groep_id) {
      $tax_queries = ['relation' => 'AND'];
      $tax_queries[] = [
        'taxonomy' => 'groepen',
        'field' => 'term_id',
        'terms' => $groep_id,
        // 'operator' => '='
      ];
      $args['tax_query'] = $tax_queries;
    }

    $query = new WP_Query($args);
    if( ! ( isset($query) && $query->have_posts() ) ) return false;

    $locations = []; $_location_ids = [];
    $ages = []; $_age_ids = [];
    $courses = [];
    foreach( $query->posts as $post) {
      $_locations = get_the_terms($post->ID, 'locaties');
      $_ages = get_the_terms($post->ID, 'leeftijden');
      // $_groepen = get_the_terms($post->ID, 'groepen'); // needed?
      $_locations = $this->extract_location_terms($_locations);
      $_ages = $this->extract_ages_terms($_ages);
      $course = [
        'id' => $post->ID,
        'title' => $post->post_title,
        'permalink' => get_the_permalink($post->ID),
        'image' => $this->getCourseImage($post->ID),
        'locations' => $_locations,
        'ages' => $_ages,
        // 'groepen' => $_groepen
      ];
      $courses[] = $course;
      $_location_ids = array_merge($_location_ids, $_locations['ids']);
      $_age_ids = array_merge($_age_ids, $_ages['ids']);
    }

    $locations = get_terms( [
      'taxonomy' => 'locaties',
      'hide_empty' => true,
      'include' => array_unique($_location_ids),
      'parent' => 0,
      'orderby' => 'name',
      'order' => 'ASC'
    ]);

    $ages = get_terms( [
      'taxonomy' => 'leeftijden',
      'hide_empty' => true,
      'childless' => true,
      'include' => array_unique($_age_ids),
    ]);

    return [
      'courses' => $courses,
      'locations' => $locations,
      'ages' => $ages
    ];
  }

}
?>