<?php 
/* Themas archive page */
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { $ajax = true; } else { $ajax = false; }
if ($ajax != true) { get_header(); }

$queried = get_queried_object(); // We should use this to display the requested thema on load
$terms = get_terms( array('themas') );
$themes = array();

// Process each thema and push or unshift it into the 'themes' array
foreach ($terms as $term) {
	if ($queried->term_id == $term->term_id) {
		array_unshift( $themes, $term ); // Prepend thema
	} else {
		array_push( $themes, $term ); // Append thema
	}
}
?>

<div class="thema__slider__header__wrapper">
	
	<div class="thema__slider__nav__wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-8-offset-2">
					<div class="thema__slider__nav"></div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="thema__slider__header">
	
		<?php foreach ( $themes as $theme ) { ?>
		<div class="thema__slider__header__slide" data-slide-button="<?php echo $theme->name; ?>" data-slide-thema="<?php echo $theme->slug; ?>">

			<?php
			$placeholder = get_field( 'thema-placeholder', 'themas_'.$theme->term_id );
			$backdrop = wp_get_attachment_image_src( $placeholder['ID'], 'animation-placeholder-visual', false );
			?>
			<div class="header__animation animation__placeholder--thema ani__opacity animation__placeholder" style="background-image: url('<?php echo $backdrop[0]; ?>');">
				<button class="animation__play__button animation__play__button--thema animation__play__button--loaded animation__play__button--player ani__all" 
						data-video-start="<?php the_field( 'thema-video-start', 'themas_'.$theme->term_id ); ?>" 
						data-video-duration="<?php the_field( 'thema-video-end', 'themas_'.$theme->term_id ); ?>">
				<?php echo $theme->name; ?>
				</button>
				<div class="vimeo__animation__wrapper"><!-- INJECT IFRAME INTO THIS CONTAINER --></div>
			</div>

		</div>
		<?php } ?>
	
	</div>
</div>

<div class="thema__slider__content">
	
	<?php foreach ( $themes as $theme ) { ?>
	
	<div class="thema__slider__content__slide">
		
		<div class="container thema__header thema__header--archive ani__opacity">
			<div class="col-md-8 col-md-offset-2 thema__header__wrapper">
				<div class="col-md-4 col-sm-3 thema__header__title"><?php echo $theme->name; ?></div>
				<div class="col-md-7 col-sm-8 thema__header__desc">
					<?php the_field( 'thema-excerpt', 'themas_'.$theme->term_id ); ?>
					<a href="#" rel="scrollto" class="more__button">Projecten</a>
				</div>
				<div class="col-md-1 col-sm-1">&nbsp;</div>
			</div>
		</div>
		
		<div class="home__projects__higlighted home__projects__higlighted--archive ani__blur">
			<div class="container">
				<div class="row">
				<?php
				$count=0;
				$themesQuery = new WP_Query( 
					array( 'post_type' => 'projecten',
						   'posts_per_page' => -1,
						   'tax_query' => array(
								array(
									'taxonomy' => 'themas',
									'field'    => 'term_id',
									'terms'    => $theme->term_id,
								),
							),
						) 
				); 
				while ( $themesQuery->have_posts() ) { $themesQuery->the_post();
				?>
					<div class="col-md-6">
						<?php
						$count++;
						get_template_part( 'components/comp', 'project' );
						?>
					</div>
				<?php
				} wp_reset_query(); 
				?>
				</div>
			</div>
		</div>
		
	</div>
			
	<?php } ?>

</div>

<?php if ($ajax != true) { get_footer(); } ?>