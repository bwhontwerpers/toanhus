<?php
/**
 * Template Name: Aanbod cursus template
 *
 * @package WordPress
 * @subpackage toanhus
 * @since toanhus 1.0
 */
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { $ajax = true; } else { $ajax = false; }
if ($ajax != true) { get_header(); }
$cursus_groep = wp_get_post_terms( get_the_ID(), 'groepen' );
$cursus_groep = array_shift( $cursus_groep );

?>

<?php while ( have_posts() ) { the_post(); ?>

<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__header__visual__wrapper">
					<button class="container__header__back" onclick="window.history.go(-1);"><i class="ani__all fa fa-chevron-left fa-fw"></i></button>
					<?php
					if ( get_field( 'header-visual' ) ) { $header = get_field( 'header-visual' ); $header = $header['ID']; } 
					else { $header = get_field( 'global-article-header', 'option' ); }
					echo wp_get_attachment_image( $header, 'page-header-visual', false, array( 'class' => 'container__header__visual' ) );
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php /* Cursus info blocks */ global $frequenties; ?>

<div class="block__cursus__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__row__wrapper">
					<div class="col-md-4 col-sm-12">
						<div class="block__cursus__wrapper">
							<?php if( get_field('extra') ) { ?>
							<div class="block__cursus__details">
									<label>Meer informatie:</label>
									<?php the_field('extra'); ?>
							</div>
							<?php } ?>
						</div>
					</div>
					<div class="col-md-8 col-sm-12">
						<div class="block__cursus__text styled__content">
							<h2 class="title title--centered title--red title--large"><?php echo bwh_cursus_title( get_the_title() ); ?></h2>
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

						
<?php get_template_part( 'components/site', 'share' ); ?>

<?php } ?>

<?php if ($ajax != true) { get_footer(); } ?>