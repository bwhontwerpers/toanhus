<?php 
/* Aanbod - gefilterd overzicht */ 
$aanbod = AanbodService::i();
if ( $aanbod->hasFilter('filter-locaties') || $aanbod->hasFilter('filter-leeftijden') || $aanbod->hasFilter('filter-groepen') ) {
	include("archive-cursussen-filtered.php");
	die();
}
get_header();
?>

<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__header__visual__wrapper">
					<button class="container__header__back" onclick="window.history.go(-1);"><i class="ani__all fa fa-chevron-left fa-fw"></i></button>
					<?php echo wp_get_attachment_image( get_field( 'global-groepen-header-visual','option' ), 'page-header-visual', false, array( 'class' => 'container__header__visual' ) ); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="block__category__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__row__wrapper no-gutter">
					<div class="col-md-6 col-sm-6">												
						<div class="block__category__title">
							<h2 class="title title--light title--medium title--large"><?php the_field( 'global-groepen-header-title', 'option' ); ?></h2>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="block__category__text">
							<?php the_field( 'global-groepen-header-text', 'option' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<?php get_template_part( 'components/site', 'aanbod-blokken' ); ?>
			</div>
		</div>
	</div>
</div>

<div class="container container__contains__finder">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper no-gutter">
				<div class="col-md-6">
					<div class="block__intro matchheight">
						<?php the_field( 'global-cursusfinder-text', 'option' ); ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="block__aanbod matchheight">
						<h2 class="title title--light">Vind je cursus of les</h2>
						<button class="comp__start__finder"><i class="fa fa-search ani__all"></i><span class="ani__all">Start cursusvinder</span></button>
					</div>
					<?php get_template_part( 'components/site', 'cursus-finder' ); ?>
				</div>

			</div>
		</div>
	</div>
</div>

<?php get_template_part( 'components/site', 'highlights' ); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">&nbsp;</div>		
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>