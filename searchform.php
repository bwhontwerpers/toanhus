<div class="search__bar ani__drawer">
	<form role="search" method="get" id="searchform" class="search__bar__form" action="/">
		<div>
			<input type="text" value="<?php if ( count( $_GET ) > 0 ) { echo $_GET['s']; } ?>" name="s" id="s" class="search__bar__field" placeholder="Zoeken naar lessen, cursussen en informatie">
			<input type="hidden" name="post_type" value="cursussen" />
			<button type="submit" class="search__bar__submit ani__all"><i class="fa fa-fw fa-search"></i></button>
		</div>
	</form>
</div>