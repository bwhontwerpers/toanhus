<?php
/* Single cursus template */
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { $ajax = true; } else { $ajax = false; }
if ($ajax != true) { get_header(); }

$cursus_groep = wp_get_post_terms( get_the_ID(), 'groepen' );
$cursus_groep = array_shift( $cursus_groep );
?>
<?php while ( have_posts() ) { the_post(); ?>

<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__header__visual__wrapper">
					<button class="container__header__back" onclick="window.history.go(-1);"><i class="ani__all fa fa-chevron-left fa-fw"></i></button>
					<?php
						$header = get_field( 'header-visual' );
						$poster = get_the_post_thumbnail(null, 'large');

						if( isset($header) && !empty($header['ID']) ) : ?>
							<img src="<?= $header['sizes']['large']; ?>" alt="<?= $header['alt']; ?>">
						<?php else : ?>
							<?= $poster; ?>
						<?php endif; ?>
						<?php
						// if( !empty($poster)) {
						// 	// poster
						// 	echo $poster;
						// } else {
						// 	// header-visual

						// 	if( !empty($header) && isset($header['ID']) ) {
						// 		echo wp_get_attachment_image( $header['ID'], 'page-header-visual', false, array( 'class' => 'container__header__visual' ) ); 
						// 	} else {
						// 		// groep-header-visual
						// 		$headerId = get_field( 'groep-header', 'groepen_'.$cursus_groep->term_id );
						// 		if( !empty($headerId) ) {
						// 			echo wp_get_attachment_image( $headerId, 'page-header-visual', false, array( 'class' => 'container__header__visual' ) ); 
						// 		} else {
						// 			// default visual
						// 			$headerId = get_field( 'global-aanbod-default-header', 'option' );
						// 			echo wp_get_attachment_image( $headerId, 'page-header-visual', false, array( 'class' => 'container__header__visual' ) ); 
						// 		}
						// 	}
						// }
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
if ( has_term( 'instrumenten', 'groepen', $post ) ) { 
	get_template_part( 'components/site', 'les-info' );
} else {
	get_template_part( 'components/site', 'cursus-info' );
}
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">&nbsp;</div>
			</div>
		</div>
	</div>
</div>

<?php
if ( has_term( 'instrumenten', 'groepen', $post ) ) { 
	get_template_part( 'components/site', 'les-roosters' );
} else {
	get_template_part( 'components/site', 'cursus-roosters' );
}
?>

<div class="block__form__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__form__wrapper">
					<div class="col-md-12">
						<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=30 field_values="type_inschrijving=cursus"]'); ?>
					</div>
				</div>
			</div> 
		</div>
	</div>
</div>

<?php get_template_part( 'components/site', 'share' ); ?>

<?php } ?>

<?php if ($ajax != true) { get_footer(); } ?>