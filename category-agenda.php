<?php 
/* Category - agenda */ 
get_header();
?>

<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__header__visual__wrapper">
					<button class="container__header__back" onclick="window.history.go(-1);"><i class="ani__all fa fa-chevron-left fa-fw"></i></button>
					<?php
					if ( get_field( 'header-visual' ) ) { $header = get_field( 'header-visual' ); $header = $header['ID']; } 
					else { $header = get_field( 'global-article-header', 'option' ); }
					echo wp_get_attachment_image( $header, 'page-header-visual', false, array( 'class' => 'container__header__visual' ) );
					?>
					<h2 class="news__article__title"><?php echo single_cat_title(); ?></h2>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">			
				<?php
				while ( have_posts() ) { 
					the_post();
					?>
					<div class="col-md-12">
						<a class="loop__event__item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<time class="loop__event__date" datetime="<?php echo date_i18n( 'Y-m-d h:i', strtotime($post->post_date_gmt), true ); ?>">
								<span class="day ani__all"><?php echo date_i18n( 'D', strtotime($post->post_date_gmt), true ); ?></span>
								<span class="date ani__all"><?php echo date_i18n( 'd', strtotime($post->post_date_gmt), true ); ?></span>
								<span class="month ani__all"><?php echo date_i18n( 'M', strtotime($post->post_date_gmt), true ); ?></span>
							</time>
							<h3 class="title title--red title--normal title--large loop__event__title">
								<?php the_title(); ?>
							</h3>
							<div class="loop__event__description">
								<?php the_excerpt(); ?>
							</div>
						</a>
					</div>	
					<?php
				} wp_reset_query();
				?>
			</div>
		</div>
	</div>
</div>

<?php get_template_part( 'components/site', 'highlights' ); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">&nbsp;</div>		
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>