/*
VARIABLE DEFINITIONS
---------------------------------------------------------------------------------------------------
*/
var ajaxurl;


function disable_sticky_announcements(target) {
	'use strict';
	
	jQuery.post(the_ajax_script.ajaxurl,
		{
			action: "bwh_hide_announcements_hook",
			output: 'json'
		}, 
		function(results) {
			results = JSON.parse(results);
			//console.log(results);
		}
	);
	
	jQuery(target).parent().slideUp(250);
	
}


function match_column_heights() {
	'use strict';
	
	var matchheight_options = {
	    byRow: true,
	    property: 'height',
	    target: null,
	    remove: false
	}

	jQuery('.matchheight').matchHeight(matchheight_options);
	
}

function init_waypoints() {
	'use strict';
	
	if ( 'fixed' == jQuery('.header__wrapper').css('position') ) {
		
		var waypoints = jQuery('.dynamic').waypoint({
			offset: '0',
			handler: function(direction) {
				if ('down' == direction) {
					jQuery('body').addClass('header--sticky--inverted');
				} else {
					jQuery('body').removeClass('header--sticky--inverted');
				}

			}
		});
		
	}
	
}

function toggle_search_bar() {
	'use strict';
	
	jQuery('.header__search__button').on('click', function() {
		if (!jQuery('body').hasClass('search--visible')) { jQuery('.search__bar__field').focus(); }
		jQuery('.header__menu__button i').removeClass('fa-times').addClass('fa-bars');
		jQuery('body').removeClass('canvas--visible');
		jQuery('body').toggleClass('search--visible');
		jQuery(this).find('i').toggleClass('fa-search').toggleClass('fa-times');
	});
	
}

function start_highlights_slider() {
	'use strict';
	
	jQuery('.highlights__slider').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
	    var i = (currentSlide ? currentSlide : 1);
	    jQuery('.highlights__slider__counter .count').text(i + '/' + (slick.slideCount-2));
	});

	jQuery('.highlights__slider').slick({
		arrows: false,
		respondTo: 'window',
		adaptiveHeight: false,
		variableWidth: true,
		infinite: false,
		initialSlide: 1,
		speed: 750,
		autoplay: true,
		autoplaySpeed: 4500,
		useTransform: true,
		cssEase: 'cubic-bezier(0.280, 0.585, 0.475, 1.000)',
		focusOnSelect: false,
		centerMode: true,
	    centerPadding: '36px',
	    responsive: [
		    {
		      breakpoint: 767,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        centerMode: false,
		        variableWidth: false,
		      }
		    }
		  ]
	});
	
	setTimeout(function() { 
		jQuery('.highlights__slider').slick('slickNext');
	},1000);

}


function toggle_cursus_finder() {
	'use strict';
	
	jQuery('.comp__start__finder').not('.comp__start__finder--finder').not('.comp__start__finder--alt').on('click', function(event) {

		if (!jQuery('body').hasClass('finder--visible')) {
			jQuery('html, body').animate(
				{ scrollTop:jQuery('.block__aanbod').offset().top-36 },
				500
			);
		}

		jQuery('body').toggleClass('finder--visible');		
		event.preventDefault();
	});
	
	jQuery('.comp__close__finder').on('click', function(event) { 
		jQuery('body').toggleClass('finder--visible');
		event.preventDefault();
	});
	
}

function cursus_finder_calculate() {
	'use strict';
	
	jQuery('form[name=block__aanbod__finder__form]').on('change', function() {
		
		var locaties = new Array();
		var leeftijden = new Array();
		var groepen = new Array();
		
		jQuery('form[name=block__aanbod__finder__form] input[name^="filter-locaties"]:checked').each(function(key,value) {
			locaties.push(jQuery(value).val());
		});

		jQuery('form[name=block__aanbod__finder__form] input[name^="filter-leeftijden"]:checked').each(function(key,value) {
			leeftijden.push(jQuery(value).val());
		});

		jQuery('form[name=block__aanbod__finder__form] input[name^="filter-groepen"]:checked').each(function(key,value) {
			if ( jQuery(this).parent().hasClass('aanbod__finder__filters__item--sub') ) {
				if ( jQuery.inArray(jQuery(this).parent().attr('data-child-parent'), groepen ) >= 0 ) {
					var parent_array_key = jQuery.inArray(jQuery(this).parent().attr('data-child-parent'), groepen );
					groepen.splice(parent_array_key,1);
				}
			}
			groepen.push(jQuery(value).val());
		});
		
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			dataType: 'json',
			data: {"action": "bwh_cursus_finder_calculate_hook", "locaties":locaties,"leeftijden":leeftijden,"groepen":groepen},
			success: function(results) {
				
				if (0 < results.count) {
					jQuery('.comp__start__finder--finder').removeClass('comp__start__finder--disabled').find('span').text('Bekijk '+results.count+' resultaten');
				} else {
					jQuery('.comp__start__finder--finder').addClass('comp__start__finder--disabled').find('span').text('Geen resultaten gevonden');
				}
			}
		});
		
	});
	
}

function toggle_category_menu() {
	'use strict';
	
	jQuery('.loop__cursus__item--category').on('click', function() {
		jQuery('.loop__cursus__item--category').removeClass('current');
		jQuery(this).toggleClass('current');
	});
}

function init_signup_procedure() {
	'use strict';
	
	// Handle signup button (show roster form)
	jQuery("button[data-signup-type='cursus']").on("click", function() {
		// jQuery('.block__roosters__wrapper').slideDown(250);
		jQuery('body,html').animate({'scrollTop':(jQuery('.block__roosters__wrapper').offset().top-36)},500);
		
		// If only one roster is displayed, make it active
		if ( 1 == jQuery('.block__roosters__wrapper .block__roosters__list li').length ) {
			jQuery('.block__roosters__wrapper .block__roosters__list li input[type=radio]').prop('checked','checked').trigger('click');
		}
		
	});
	
	jQuery("button[data-signup-type='les']").on("click", function() {
		// get_available_blokvormen();
		toanhus_generate_rosters();
		//jQuery('.gform_fields .block__roosters__list').hide();
		// jQuery('.block__roosters__wrapper').slideDown(250);
		jQuery('body,html').animate({'scrollTop':(jQuery('.block__roosters__wrapper').offset().top-36)},500);
		
		// If only one roster is displayed, make it active
		if ( 1 == jQuery('.block__roosters__wrapper .block__roosters__list li').length ) {
			jQuery('.block__roosters__wrapper .block__roosters__list li input[type=radio]').prop('checked','checked').trigger('click');
		}
		
	});
	
	// Handle roster selection (enable next button)
	jQuery('body').on('click', 'ul.block__roosters__list li', function() {		
		jQuery(".block__roosters__wrapper button.button--advanced[disabled=disabled]").removeAttr("disabled");
	});
	
	jQuery('body').on('click', '.block__roosters__list .gfield_radio li', function() {
		jQuery(".block__roosters__wrapper button.button--advanced[disabled=disabled]").removeAttr("disabled");
	});
	
	// Handle roster next button (show form and update values)
	jQuery(".block__roosters__wrapper button.button--advanced").on("click", function() {
		
		// Update hidden values
		if ( jQuery('ul.block__roosters__list input[name=blokvorm]:checked').length ) {
			jQuery('input[name=input_36]').val( jQuery('ul.block__roosters__list input[name=blokvorm]:checked').attr('data-roster-id') );
			jQuery('input[name=input_38]').val( jQuery('ul.block__roosters__list input[name=blokvorm]:checked').attr('data-roster-desc') );
			/*jQuery('input[name=input_49]').val( jQuery('ul.block__roosters__list input[name=blokvorm]:checked').attr('data-roster-startdate') );*/
		}
		
		if ( jQuery('.block__roosters__list .gfield_radio input:checked').length ) {
			
			jQuery('input[name=input_36]').val( jQuery('.block__roosters__list .gfield_radio input:checked').val() );
			jQuery('input[name=input_39]').val( jQuery('#gform_wrapper_4 select[name=input_6] :selected').text() );
			jQuery('input[name=input_40]').val( jQuery('#gform_wrapper_4 select[name=input_1]').val() );
			jQuery('input[name=input_41]').val( jQuery('#gform_wrapper_4 select[name=input_4]').val() );
			jQuery('input[name=input_42]').val( jQuery('#gform_wrapper_4 select[name=input_2] :selected').text() );
			jQuery('input[name=input_43]').val( jQuery('#gform_wrapper_4 select[name=input_5]').val() );
			
		}		
		
		// Show form
		jQuery(".block__roosters__wrapper").addClass("step__done");
		jQuery(".container__form__wrapper").slideDown(250);
		
		// Scroll to form
		jQuery('body,html').animate({'scrollTop':jQuery(".gform_page:visible .gform_page_fields").offset().top-36},500);
		
	});
	
	// Intercept 'back' button in form to show/hide previous/current pages
	jQuery('body').on('click', '.gform__next__button--prev', function(event) {
		
		jQuery('#gform_page_1_1').show();
		jQuery('#gform_page_1_2').hide();
		event.preventDefault();
	});

	// Intercept 'next' button in form to scroll to form
	jQuery('body').on('click', '.gform__next__button--next', function(event) {	
		setTimeout(
			function() { jQuery('body,html').animate({'scrollTop':jQuery('#gform_page_1_2').offset().top-50}, 500); },
			500
		);		
	});

	jQuery('body').on('submit', 'form.inschrijven__form', function(event) {
		jQuery('.gform__next__button--prev').hide();
		jQuery('.gform__next__button--finish').prop('disabled',true).addClass('processing');
		jQuery('.gform__next__button--finish span').text('Bezig met verwerken');
	});

	// INIT 
	toanhus_generate_rosters();
	// jQuery("button[data-signup-type='les']").trigger("click");
		
}

function get_available_blokvormen() {
	'use strict';
	//console.log("Fetching available blokvormen");
}

function init_clipboard_links() {
	'use strict';
	try {
		new Clipboard('a.clipboard');
	} catch (e) {
		// not compatible with Chrome
	}
}

function init_form_copy_cat() {
	'use strict';
	
	if ( jQuery('#gform_wrapper_1').length ) { // Check if form exists
					
		// Fetch all fields that have a copy class and hook a copy class to it to migrate data on change
		jQuery('#gform_wrapper_1 li[class*=copy] input').each(function(nr,el) {
			jQuery(this).on('change', function() {
				var copycat_class = jQuery.grep(jQuery(this).parent().parent().attr('class').split(" "), function(v, i) { return v.indexOf('copy') === 0; }).join();
				var copycat_args = copycat_class.split("-");
				var copycat_target = jQuery('#gform_wrapper_1 li#field_1_'+copycat_args[3]).find('input');
				if ( !jQuery(copycat_target).val().length ) {
					jQuery(copycat_target).val(jQuery(this).val()); // Copy value, only if target field has no value
				}
			});
		});
		
	}
	
}

function init_mobile_menu() {
	'use strict';
	
	jQuery('.header__menu__button').on('click', function() {
		
		jQuery('body').toggleClass('canvas--visible');
		jQuery('body').removeClass('search--visible');
		jQuery('.header__search__button i').addClass('fa-search').removeClass('fa-times');
		jQuery(this).find('i').toggleClass('fa-bars').toggleClass('fa-times');
		
	});
}

function init_load_maps() {
	'use strict';
	
	if ( jQuery('.container__maps').length ) { 
		
		var mapsWrapper = jQuery(".container__maps");
		var map = false;
        var bound = new google.maps.LatLngBounds();
	    var boundExtended = false;

        var mapsLat 	= '52.9621082';
		var mapsLng 	= '5.7999577';
        var mapsObject  = document.getElementsByClassName("container__maps");

		var mapsLatLng 	= new google.maps.LatLng(mapsLat,mapsLng);
		
		var mapStyling  = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}];

        map = new google.maps.Map(mapsObject[0], {
                center: mapsLatLng,
                zoom: 14,
                styles: mapStyling,
                panControl: true,
                draggable: true,
                zoomControl: true,
                scrollwheel: false,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                overviewMapControl: false,
                disableDoubleClickZoom: false,
            });

        /* Set marker of current object */
		var mapIcon = {
				url: '/wp-content/themes/toanhus/lib/img/th_marker.svg',
		};
		
		var beachMarker = new google.maps.Marker({
			position: mapsLatLng,
			map: map,
			icon: mapIcon
		});
		/*
        var marker = new MarkerWithLabel({
            position: mapsLatLng,
            draggable: false,
            raiseOnDrag: false,
            map: map,
            labelContent: mapsLabel,
            labelAnchor: new google.maps.Point(-13, 13),
            labelClass: "comp_marker_label", // the CSS class for the label
            labelStyle: {opacity: 1.0},
        });
        */

        //bound.extend(marker.getPosition());
		
	}
	
}

/*
EVENT LISTENERS
---------------------------------------------------------------------------------------------------
*/
	
	jQuery(window).resize(function() {
		'use strict';
		
		match_column_heights();
		
	});
	
	
	jQuery(document).ready(function() {
		'use strict';
		
		cursus_finder_calculate();
		match_column_heights();
		toggle_search_bar();
		toggle_cursus_finder();
		start_highlights_slider();
		toggle_category_menu();
		
		init_signup_procedure();
		init_clipboard_links();
		init_waypoints();
		init_form_copy_cat();
		init_mobile_menu();
		init_load_maps();
		
		jQuery(".sound-toggle").on('click', function(e) {
			jQuery(this).toggleClass('muted');
			jQuery('.video-element video').prop("muted", !jQuery('.video-element video').prop("muted"));
		});


	});
	
	
	jQuery(window).load(function() {
		'use strict';
		
		

	});