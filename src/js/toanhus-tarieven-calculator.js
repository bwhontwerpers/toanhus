Number.prototype.formatMoney = function(c, d, t){
var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };
 
function toanhus_update_calculator() {
	'use strict';
	
	var filter_leeftijd = jQuery('#gform_2 #input_2_1').val();
	var filter_lesvorm = jQuery('#gform_2 #input_2_2').val();
	var filter_minuten = jQuery('#gform_2 #input_2_4').val();
	var filter_frequentie = jQuery('#gform_2 #input_2_5').val();	
	var filter_cursus = jQuery('#gform_2 #input_2_7').val();
	var filter_lessen = jQuery('#gform_2 #input_2_9').val();
	var filter_hafa = jQuery('#gform_2 #input_2_6').val();
	
	// Disable fields
	//jQuery('#input_2_6,#input_2_1,#input_2_2,#input_2_4,#input_2_5').animate({'opacity':.5}, 500).attr('disabled','disabled');
	jQuery('#input_2_6,#input_2_1,#input_2_2,#input_2_4,#input_2_5').animate({'opacity':.5}, 150).attr('disabled','disabled');
	
	// Enable reset
	jQuery('.block__cursus__reset').addClass('active');
	
	//console.log("Updating dropdowns and calculating price");
	
	jQuery.ajax({
		    type: "POST",
		    url: ajaxurl,
		    cache: false,
		    dataType: 'json',
		    data: ({
			    action:'toanhus_update_gf_calculator_fields_hook',
			    leeftijd:filter_leeftijd,
			    lesvorm:filter_lesvorm,
			    minuten:filter_minuten,
			    frequentie:filter_frequentie,
			    hafa:filter_hafa,
			    cursus: filter_cursus,
			}),
		    success: function(data) {
			    if (data) { // json result set with new dropdown values for each box
				    				    			    
				    jQuery('#input_2_1').empty();
				    jQuery('#input_2_1').append('<option value="" placeholder>Alle leeftijden</option>');
				    jQuery(data.leeftijden).each(function(key) { 
					    var option = data.leeftijden[key];
						jQuery('#input_2_1').append('<option value="'+option.value+'" '+option.selected+'>'+option.text+'</option>');					    
				    });
				    
				    jQuery('#input_2_2').empty();
				    jQuery('#input_2_2').append('<option value="" placeholder>Alle lesvormen</option>');
				    jQuery(data.lesvorm).each(function(key) { 
					    var option = data.lesvorm[key];
						jQuery('#input_2_2').append('<option value="'+option.value+'" '+option.selected+'>'+option.text+'</option>');					    
				    });
				    
				    jQuery('#input_2_4').empty();
				    jQuery('#input_2_4').append('<option value="" placeholder>Alle minuten</option>');
				    jQuery(data.minuten).each(function(key) { 
					    var option = data.minuten[key];
						jQuery('#input_2_4').append('<option value="'+option.value+'" '+option.selected+'>'+option.text+'</option>');					    
				    });
				    
				    jQuery('#input_2_5').empty();
				    jQuery('#input_2_5').append('<option value="" placeholder>Alle frequenties</option>');
				    jQuery(data.frequentie).each(function(key) { 
					    var option = data.frequentie[key];
						jQuery('#input_2_5').append('<option value="'+option.value+'" '+option.selected+'>'+option.text+'</option>');					    
				    });
				    
				    //console.log(data.price);
				    if ( 0 != data.price ) {
						
					    // console.log("frequentie: " + filter_frequentie);
						var total_price = parseFloat(data.price.replace(',', '.'));
						var les_price;

						if(!data.aantallessen || !data.aantallessen || !data.aantallessen || !data.aantallessen || !data.aantallessen){
							les_price = parseFloat(data.price.replace(',', '.'))/parseFloat(filter_lessen);
							// console.log("aantal lessen:"+filter_lessen);

							// If frequent is provided (only 1 frequentie is returned) and frequentie equals 2wk (een keer per twee weken), cut les price in half					    
							if ( 1 == price_freq.length && '2wk' == price_freq[0].value ) {
								les_price = les_price * 2;
								// console.log("2 wekelijks dus prijs per les wordt verdubbeld");								
							}
													
						} else {
							les_price = parseFloat(data.price.replace(',', '.'))/parseFloat(data.aantallessen);
							// console.log("aantal lessen:"+data.aantallessen);	
						}
					    var price_freq = data.frequentie;
						
					    var btw_plichtig = '';
					    if ( 1 == data.btw ) {
						    btw_plichtig = "<br/><small>Cursusbedrag is inclusief 21% BTW. Cursisten jonger dan 21 jaar betalen geen BTW.</small>";
					    } 

							var leeftijd_melding = '';
							var leeftijd = jQuery('#input_2_1').val();
							if( (leeftijd != undefined) && (leeftijd.indexOf('jonger dan 18') >= 0 ) ) {
								leeftijd_melding = "<br/><small>De tarieven tot 18 jaar gelden alleen voor inwoners van De Fryske Marren</small>";
							}
					    
					    jQuery( '#price ').html('Prijs per les&nbsp;&nbsp;<strong>&euro; '+(les_price).formatMoney(2, ',', '.')+'</strong> &nbsp;&nbsp;|&nbsp;&nbsp; Totaal&nbsp;&nbsp;<strong>&euro; '+(total_price).formatMoney(2, ',', '.')+'</strong>'+btw_plichtig+leeftijd_melding);
				    } else {
					    jQuery( '#price ').html('Bereken de prijs voor deze cursus');
				    }
				    
				    jQuery('#input_2_6,#input_2_1,#input_2_2,#input_2_4,#input_2_5').animate({'opacity':1}, 150).removeAttr('disabled');
				    
				}
			}
	});
	
	// Enable fields
	
	
	// If all fields are filled, fetch the updated amount
	/*
	if (filter_leeftijd && filter_lesvorm && filter_minuten && filter_frequentie) {
		
		jQuery.ajax({
		    type: "POST",
		    url: ajaxurl,
		    cache: false,
		    dataType: 'json',
		    data: ({
			    action:'toanhus_finish_gf_calculator_hook',
			    leeftijd:filter_leeftijd,
			    lesvorm:filter_lesvorm,
			    minuten:filter_minuten,
			    frequentie:filter_frequentie,
			    cursus: filter_cursus,
			}),
		    success: function(data) {
			    if (data) {
				    console.log(data);
				}
			}
		});
		
	}
	*/
	
}

function toanhus_generate_rosters() {
	'use strict';
	
	var filter_leeftijd = jQuery('#gform_4 #input_4_1').val();
	var filter_lesvorm = jQuery('#gform_4 #input_4_2').val();
	var filter_minuten = jQuery('#gform_4 #input_4_4').val();
	var filter_frequentie = jQuery('#gform_4 #input_4_5').val();	
	var filter_cursus = jQuery('#gform_4 #input_4_7').val();
	var filter_lessen = jQuery('#gform_4 #input_4_9').val();
	
	// Disable fields
	jQuery('#input_4_6,#input_4_1,#input_4_2,#input_4_4,#input_4_5').animate({'opacity':.5}, 150).attr('disabled','disabled');
	
	// console.log("Updating rosters");
	
	// console.log(filter_leeftijd);
	// console.log(filter_lesvorm);
	// console.log(filter_minuten);
	//console.log(filter_frequentie);
	//console.log(filter_lessen);
	//console.log(filter_cursus);
	
	
	jQuery.ajax({
		    type: "POST",
		    url: ajaxurl,
		    cache: false,
		    dataType: 'json',
		    data: ({
			    action:'toanhus_update_gf_calculator_fields_hook',
			    leeftijd:filter_leeftijd,
			    lesvorm:filter_lesvorm,
			    minuten:filter_minuten,
			    frequentie:filter_frequentie,
			    cursus: filter_cursus,
			}),
		    success: function(data) {
			    
			    //console.log("Executed update_calculater_fields_hook");
			    
			    if (data) { // json result set with new dropdown values for each box
				    
				    //console.log("Got data from toanhus_update_gf_calculator_fields_hook");
				    //console.log(data.price);

				    jQuery('#input_4_1').empty();
				    jQuery('#input_4_1').append('<option value="" placeholder>Alle leeftijden</option>');
				    jQuery(data.leeftijden).each(function(key) { 
					    var option = data.leeftijden[key];
						jQuery('#input_4_1').append('<option value="'+option.value+'" '+option.selected+'>'+option.text+'</option>');					    
				    });
				    
				    jQuery('#input_4_2').empty();
				    jQuery('#input_4_2').append('<option value="" placeholder>Alle lesvormen</option>');
				    jQuery(data.lesvorm).each(function(key) { 
					    var option = data.lesvorm[key];
						jQuery('#input_4_2').append('<option value="'+option.value+'" '+option.selected+'>'+option.text+'</option>');					    
				    });
				    
				    jQuery('#input_4_4').empty();
				    jQuery('#input_4_4').append('<option value="" placeholder>Alle minuten</option>');
				    jQuery(data.minuten).each(function(key) { 
					    var option = data.minuten[key];
						jQuery('#input_4_4').append('<option value="'+option.value+'" '+option.selected+'>'+option.text+'</option>');					    
				    });
				    
				    jQuery('#input_4_5').empty();
				    jQuery('#input_4_5').append('<option value="" placeholder>Alle frequenties</option>');
				    jQuery(data.frequentie).each(function(key) { 
					    var option = data.frequentie[key];
						jQuery('#input_4_5').append('<option value="'+option.value+'" '+option.selected+'>'+option.text+'</option>');					    
				    });
				    console.log('Nu in de loop om tarieven aan te passen met als tariefnummer:'+data.tarieven);
					
					jQuery('.block__roosters__list .ginput_container_radio .gfield_radio').html('');
					var rooster_counter = 0;
					jQuery(data.roosters).each(function(key) {
						rooster_counter++;
						var rooster = data.roosters[key];
						
						if (rooster) {
							var rooster_option = '<li class="gchoice_4_11_'+rooster_counter+'"><input name="input_11" type="radio" value="'+rooster.value+'" id="choice_4_11_'+rooster_counter+'" tabindex="26"><label for="choice_4_11_'+rooster_counter+'" id="label_4_11_0"><span>'+rooster.text+'</span></label></li>';							
							jQuery('.block__roosters__list .ginput_container_radio .gfield_radio').append(rooster_option);

						} else {
							console.log('Fout: er is geen roostercombinatie gevonden!');
						}
						
					});
					
					if (data.price) {
						//jQuery('.roosters__price').html('&euro; ' + data.price).fadeTo(250,1);	
					} else {
						//jQuery('.roosters__price').html('&euro; 0,00').fadeTo(250,0);
					}
					
					if (0 < rooster_counter) {
						jQuery('.block__roosters__list').slideDown(250);
					}
					
				    /*
				    if ( 0 != data.price ) {
					    var total_price = parseFloat(data.price);
					    var les_price = parseFloat(data.price)/parseFloat(filter_lessen);
					    
					    jQuery( '#price ').html('Prijs per les&nbsp;&nbsp;<strong>&euro; '+les_price.toPrecision(4)+'</strong> &nbsp;&nbsp;|&nbsp;&nbsp; Totaal&nbsp;&nbsp;<strong>&euro; '+total_price.toPrecision(4)+'</strong>');
				    } else {
					    jQuery( '#price ').html('Bereken de prijs voor deze cursus');
				    }
				    */
				    
				    jQuery('#input_4_6,#input_4_1,#input_4_2,#input_4_4,#input_4_5').animate({'opacity':1}, 150).removeAttr('disabled');
				    
				}
			}
	});
	
	// Enable fields
	
	
	// If all fields are filled, fetch the updated amount
	/*
	if (filter_leeftijd && filter_lesvorm && filter_minuten && filter_frequentie) {
		
		jQuery.ajax({
		    type: "POST",
		    url: ajaxurl,
		    cache: false,
		    dataType: 'json',
		    data: ({
			    action:'toanhus_finish_gf_calculator_hook',
			    leeftijd:filter_leeftijd,
			    lesvorm:filter_lesvorm,
			    minuten:filter_minuten,
			    frequentie:filter_frequentie,
			    cursus: filter_cursus,
			}),
		    success: function(data) {
			    if (data) {
				    console.log(data);
				}
			}
		});
		
	}
	*/
	
}

function toanhus_calculator_hooks() {
	'use strict';
	
	if ( jQuery('#gform_2').length ) {
		jQuery('#input_2_6,#input_2_1,#input_2_2,#input_2_4,#input_2_5').on('change', function() {
			toanhus_update_calculator();
		});
	}
	
	if ( jQuery('#gform_4').length ) {
		jQuery('#input_4_6,#input_4_1,#input_4_2,#input_4_4,#input_4_5').on('change', function() {
			toanhus_generate_rosters();
		});
	}
	
}

function toanhus_reset_calculator() {
	'use strict';
	
	// Reset all dropdowns
	jQuery('#input_2_1,#input_2_2,#input_2_4,#input_2_5,#input_2_6').prop('selectedIndex', 0);
		
	// Update calculator
	toanhus_update_calculator();
	
	// De-activate reset button
	setTimeout( function() { jQuery('.block__cursus__reset').removeClass('active'); }, 1500 );
	
}

jQuery(window).load(function() {
	
	toanhus_calculator_hooks();
	
});