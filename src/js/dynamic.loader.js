/*
Usage instructions:
---
Make sure your site contains a container div in which we can load 
the dynamic content. Idealy this container has an unique ID. Also, add
a #dynamic--progress div at the top of the page (just below the opening
<body> tag). This div will be used to visualy show the loading processs.

Assing the dynamic container ID to the container variable
below.

Now add the rel="dynamic" parameter to each link you want to 
load dynamicly. That's all, the content behind the <a> permalinks
will be loaded dynamicly inside the container.
*/

// Global container used to load dynamic content
var container = "#dynamic";
var loader = "#dynamic--progress";

// Handle history change events
window.addEventListener('load', function() {
	setTimeout(function() {
		window.addEventListener('popstate', function(e) {
			console.log('[postate] popstate changed');
			console.log('[postate] loading ' + location.href + ' into ' + container + ' using ' + loader);			

			//bwh_page_slider();
			//bwh_hook_close_button();

			bwh_load_content(location.href,container,loader);	
		});
	},0);
});

/*
Load dynamic page content and push it into the dynamic container
*/
function bwh_load_content(permalink,container,loader) {
	
	console.log("[load] bwh_load_content called for " + permalink);
	console.log("");
	
	if (!permalink.length) { return; } // Stop loading when no permalink is provided
	if (!jQuery(container).length) { return; } // Stop loading when loading container is not found
	if (!jQuery(container).length) { return; } // Stop loading when loading container is not found
	
	/* Initiate dynamic content load */
	bwh_load_animation(container,'start',permalink);
	
	// Top progress bar animation (start)
	jQuery(loader).animate({'width':'100%'}, 500, function() {
				
		jQuery.ajax({
		    type: "POST",
		    url: ajaxurl,
		    cache: false,
		    context: jQuery(container),
		    dataType: 'html',
		    data: ({ action: 'bwh_load_content_hook', permalink: permalink}),
		    success: function(data) {
			    if (data) {
				    
				    // Inject result data into dynamic container
					jQuery(container).html(data);
					
					// Top progress bar animation (end)
					jQuery(loader).animate({'left':'100%','opacity':0},250,
						function() { jQuery(this).css({'left':0,'width':0,'opacity':1}); }
					);
					
					// Additional callback to display the content overlay object
					if ('#overlay' == container) { bwh_toggle_content('#overlay','start'); }
					
					// End loading animation (logo)
					bwh_load_animation(container,'end',permalink);
					
					// Default callback for awesome stuff
					do_awesome_stuff(permalink,container);
					
					// Re-init the load content function so that new dynamic buttons and links
					// are re-initiated
					bwh_init_load_content();
				}
		    },
		    error: function(message) {
			    alert(message);
		    },
		});
	});	
	
}

/*
Hook dynamic load function to rel=contents and rel=subsection elements
This function calls different callbacks based on the type of linked clicked
*/
function bwh_init_load_content() {
		
	jQuery("a[rel=contents]").off("click").on("click", function(event) {
				
		var permalink = jQuery(this).attr("href"); // Get permalink
		
		container = "#dynamic";
		
		bwh_load_content(permalink,container,loader); // Load content
	
		history.pushState(false, false, permalink); // Set address-bar location
		
		event.stopPropagation();
		event.preventDefault(); // Prevent default	
	});
	
	jQuery("a[rel=subsection]").off("click").on("click", function(event) {
		
		var permalink = jQuery(this).attr("href"); // Get permalink
		
		container = "#overlay";
		bwh_load_content(permalink,container,loader); // Load content
	
		//history.pushState(false, false, permalink); // Set address-bar location
		
		event.stopPropagation();
		event.preventDefault(); // Prevent default	
	});
	
}