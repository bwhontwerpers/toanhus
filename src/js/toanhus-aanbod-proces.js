(function($){
  var $courseList = $('#CourseList');
  if (!$courseList.length) return
  
  var $locationFilter = $('#LocationFilter');
  var $ageFilter = $('#AgeFilter');
  var $noResults = $('#NoResults');

  var $resetFilter = $('#ResetFilter');
  var $aanbodGroepen = $('#AanbodGroepen');

  // used when both filters are NOT set
  // in php class="removed" should be either set or not as well on every course-item
  var $showbydefault = ($courseList.attr('data-nofilters') == 'show');

  $locationFilter.on('change', updateCourseList);
  $ageFilter.on('change', updateCourseList);
  $courseList.on('click', '.course-item', toCourse)
  $resetFilter.on('click', resetFilters);

  function toCourse(e) {
    window.location.href = $(this).attr('data-link');
  }

  function resetFilters() {
    $ageFilter.prop('selectedIndex', 0);
    $locationFilter.prop('selectedIndex', 0);
    resetCourseList();
  }

  function resetCourseList() {
    if ($showbydefault) {
      $courseList.show().find('.course-item').removeClass('removed');
    }
    else {
      $courseList.find('.course-item').addClass('removed');
      setTimeout(function () { $('#CourseList').hide(); }, 350);
    }
    $resetFilter.removeClass('active');
    $noResults.addClass('hide');
    $aanbodGroepen.show();
  }

  function updateCourseList() {
    var $location = $locationFilter.children("option:selected").val();
    var $age = $ageFilter.children("option:selected").val();
    if ($location == -1 && $age == -1) {
      resetCourseList(); return;
    }

    var counter = 0;
    $courseList.find('.course-item').each( function(index) {
      var $_this = $(this),
        _location_checked = ($location == -1),
        _age_checked = ($age == -1);

      if (!_location_checked) {
        var _locations = $_this.attr('data-locations');
        var _split_locations = _locations.split(',')
        _location_checked = (_split_locations.indexOf($location) > -1);
      }
      if (!_age_checked) {
        var _ages = $_this.attr('data-ages');
        var _split_ages = _ages.split(',')
        _age_checked = (_split_ages.indexOf($age) > -1);
      }

      if (_location_checked && _age_checked) {
        $_this.removeClass('removed');
        counter++;
      } else {
        $_this.addClass('removed');
      }

      $resetFilter.addClass('active');
      $aanbodGroepen.hide();
    });

    if (counter == 0) {
      $courseList.hide();
      $noResults.removeClass('hide');
    } else {
      $noResults.addClass('hide');
      $courseList.show();
    }
  }

})(jQuery);