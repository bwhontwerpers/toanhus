const
  gulp = require('gulp'),
  path = require('path'),
  chmod = require('gulp-chmod'),
  sass = require('gulp-sass'),
  babel = require('gulp-babel'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  util = require('gulp-util'),
  rename = require('gulp-rename'),
  autoprefixer = require('gulp-autoprefixer');

const
  sass_dist_path = './styles',
  sass_source_files = [
    './src/sass/**/*.scss'
  ],
  js_dist_path = './api/js',
  js_source_files_main = [
    './src/js/*.js'
  ];

gulp.task('js:build-main', function(){
  return gulp.src(js_source_files_main)
    .pipe( babel({presets: ['env']}))
    // .pipe( concat('main.min.js'))
    .pipe( uglify())
    .pipe( chmod(0o775))
    .pipe( gulp.dest(js_dist_path));
});

gulp.task('sass:build', function(){
  return gulp.src(sass_source_files)
    .pipe( sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe( autoprefixer({ overrideBrowserslist: ['last 2 versions'], cascade: false}))
    .pipe( chmod(0o775))
    .pipe( gulp.dest(sass_dist_path));
});

gulp.task('watch:src', function(){
  gulp.watch( js_source_files_main, gulp.series('js:build-main'));
  gulp.watch( sass_source_files, gulp.series('sass:build'));
});

gulp.task('build',gulp.series('js:build-main','sass:build'));
gulp.task('dev',gulp.series('js:build-main','sass:build','watch:src'));