<?php
/* Single cursus template */
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { $ajax = true; } else { $ajax = false; }
if ($ajax != true) { get_header(); }
?>

<?php while ( have_posts() ) { the_post(); ?>

<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__header__visual__wrapper">
					<button class="container__header__back" onclick="window.history.go(-1);"><i class="ani__all fa fa-chevron-left fa-fw"></i></button>
					<?php
					if ( get_field( 'header-visual' ) ) { $header = get_field( 'header-visual' ); $header = $header['ID']; } 
					else { $header = get_field( 'global-article-header', 'option' ); }
					echo wp_get_attachment_image( $header, 'page-header-visual', false, array( 'class' => 'container__header__visual' ) );
					?>
					<h2 class="news__article__title"><?php echo bwh_cursus_title( get_the_title() ); ?></h2>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container content-nav">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<?php
								$group = get_page_by_path( 'in-het-onderwijs' );
								$children = wp_list_pages( 'title_li=&child_of='.$group->ID.'&echo=0&depth=1' );
								if ( $children) : ?>
								    <ul class="subpage__navigation">
								        <?php echo $children; ?>
								    </ul>
								<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-10 col-md-offset-1 news__article">
							<?php the_content(); ?>
							<?php 

							$args = array(
							    'post_type'      => 'page',
							    'posts_per_page' => -1,
							    'post_parent'    => $post->ID,
							    'order'          => 'ASC',
							 );
							
							$parent = new WP_Query( $args );
							
							if ( $parent->have_posts() ) : ?>
							
							    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
							        
							        <br /><br />
							        <h3><?php the_title(); ?></h3>
							        <?php the_content(); ?>
							
							    <?php endwhile; ?>
							
							<?php endif; wp_reset_query(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
							
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-xs-12 col-sm-6 col-lg-3">&nbsp;</div>
			</div>
		</div>
	</div>
</div>


<?php } ?>

<?php if ($ajax != true) { get_footer(); } ?>