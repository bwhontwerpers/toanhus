<?php if ( bwh_show_announcements() ) { ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="header__sticky">
				<?php 
				$stickyQuery = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 5, 'category_name' => 'mededeling', 'date_query' => array( array( 'after' => '3 days ago', ), ), ) );			
				if (isset($stickyQuery)) {
					while( $stickyQuery->have_posts() ) {
						$stickyQuery->the_post();
						?>
						<a href="<?php the_permalink(); ?>" style="background-color:<?php the_field( 'announcement-background-color' ); ?>;color:<?php the_field( 'announcement-text-color' ); ?>;" class="header__sticky__message">
							<?php the_title(); ?>
							<label>Meer informatie</label>
							<button class="header__sticky__close ani_slow" onclick="disable_sticky_announcements(this); return false;"><?php echo bwh_load_svg( 'th_close_button.svg', false ); ?></button>
						</a>
						<?php
					}
					wp_reset_query(); 
				}
				?>
			</div>	
		</div>
	</div>
</div>

<?php } ?>