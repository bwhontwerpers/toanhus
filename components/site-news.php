<?php /* News listing block */ ?>

<div class="block__nieuws matchheight">
	<h2 class="title title--dark">Nieuws</h2>
	<ul class="nieuws__articles">
		<?php
		$articlesQuery = new WP_Query( array( 'category_name' => 'nieuws', 'no_found_rows' => true, 'update_post_term_cache' => false, 'update_post_meta_cache' => false, 'post_type' => 'post', 'posts_per_page' => 3, ) );
		while ( $articlesQuery->have_posts() ) { 
			$articlesQuery->the_post();
			?>
			<li class="nieuws__articles__article">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<h3><?php the_title(); ?><time datetime="<?php echo date_i18n( 'Y-m-d h:i', strtotime($post->post_date_gmt), true ); ?>"><?php echo date_i18n( 'd/m/y', strtotime($post->post_date_gmt), true ); ?></time></h3>
				<?php the_excerpt(); ?>
				</a>
			</li>
			<?php
		} wp_reset_query();
		?>
	</ul>		
</div>
