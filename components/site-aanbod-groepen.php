<?php
  // Depandacy: $aanbod en $groep from taxonomy-groep.php
?>
<div id="AanbodGroepen" class="container aanbod-groepen">
  <div class="row">
    <div class="col-md-12">
      <div class="container__row__wrapper">
        <div class="col-md-12 aanbod-children">
          <?php foreach( $groep->children as $subgroep ) : ?>
              <div class="loop__cursus__item loop__cursus__item--category matchheight" onclick="document.location='<?= get_term_link($subgroep->term_id); ?>';">
                <h3 class="title title--light title--shadow title--normal loop__cursus__title noarrow"><?= $subgroep->name; ?></h3>
                <img src="<?= $aanbod->getImage($subgroep->term_id)[0]; ?>" />
              </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</div>