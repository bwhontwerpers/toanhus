<?php
  // Depandacy: $aanbod en $groep from taxonomy-groep.php
  $data = $aanbod->getAanbod($groep->id);
?>
<!-- filters -->
<div class="container aanbod-courses">
  <div class="row">
    <div class="col-md-12">
      <div class="container__row__wrapper">

        <div class="col-md-6 filter filter-locations">
          <select id="LocationFilter" name="LocationFilter">
            <option value="-1" selected="true">Locaties</option>
            <?php foreach( $data['locations'] as $location ) : ?>
              <option value="<?= $location->term_id; ?>">
                <?= $location->name; ?>
              </option>
            <?php endforeach; ?>
          </select>
        </div>

        <div class="col-md-5 filter filter-ages">
          <select id="AgeFilter" name="AgeFilter">
            <option value="-1" selected="true">Leeftijden</option>
            <?php foreach( $data['ages'] as $age ) : ?>
              <option value="<?= $age->term_id; ?>">
                <?= $age->name; ?>
              </option>
            <?php endforeach; ?>
          </select>
        </div>
          
        <div class="col-md-1 filter">
          <button id="ResetFilter" class=""><!-- class="active" --></button>
        </div>

      </div>
    </div>
  </div>
  <!-- no results -->
  <div id="NoResults" class="row hide">
    <div class="col-md-12">
      <div class="container__row__wrapper">
        <div class="col-sm-12 col-sm-offset-0 col-md-offset-4 col-md-4">
          <div class="no-courses-found">
            Er zijn geen cursussen voor deze locatie en leeftijdscategorie.
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- courselist  -->
  <div id="CourseList" data-nofilters="<?= $groep->is_parent ? 'hide' : 'show'; ?>" class="row aanbod-courselist">
    <div class="col-md-12">
      <div class="container__row__wrapper">
        <div class="col-md-12">
        <?php foreach( $data['courses'] as $course ) : ?>
          <div class="course-item<?= $groep->is_parent ? ' removed' : ''; ?>"
            data-locations="<?= implode(',',$course['locations']['ids']); ?>"
            data-ages="<?= implode(',',$course['ages']['ids']); ?>"
            data-link="<?= $course['permalink']; ?>">
            <?= $course['image']; ?>
            <h4><?= $course['title']; ?></h4>
            <span class="location"><i class="fa fa-building"></i>&nbsp;&nbsp;<?= implode(', ',$course['locations']['names']); ?></span><br/>
            <span class="age"><i class="fa fa-user"></i>&nbsp;&nbsp;<?= implode(', ',$course['ages']['names']); ?></span>
          </div>
        <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>

</div>