<?php /* Cursus info blocks */ global $frequenties; ?>

<div class="block__cursus__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__row__wrapper">
					<div class="col-md-6 col-sm-12">
						<div class="block__cursus__wrapper">
							<button class="block__cursus__inschrijven" data-signup-type="cursus"><i class="fa fa-fw fa-pencil-square-o ani__all"></i><span class="ani__all">Lestijden & inschrijven</span></button>
							<?php 
								//
								//	COPY "PROEFLES-BUTTON-CHANGES" TOT SITE-LES-INFO!
								//
								$proefles = false;
								$terms = wp_get_post_terms( get_the_ID(), 'groepen' );
								$has_proeflessen = [
									'dans' => ['discipline' => 'Dans', 'parameter' => 'dans'],
									'instrumenten' => ['discipline' => 'Muziek-Instrumenten', 'parameter' => 'instrument'],
									'groepen' => ['discipline' => 'Muziek-Groepen', 'parameter' => 'groepen'],
									'theater' => ['discipline' => 'Theater', 'parameter' => 'theater']
								];

								foreach( $terms as $term ) {
									if( array_key_exists($term->slug, $has_proeflessen) ) {
										$proefles = $has_proeflessen[$term->slug];
										break;
									} elseif( !empty($term->parent) ) {
										$parent = get_term_by('term_id', $term->parent, 'groepen');
										if( array_key_exists($parent->slug, $has_proeflessen) ) {
											$proefles = $has_proeflessen[$parent->slug];
											break;
										}
									}
								}

								if ( false !== $proefles ) : 
									global $post;
									$parameters = "?discipline={$proefles['discipline']}&{$proefles['parameter']}={$post->post_name}"; ?>
									<a href="/proefles-aanvragen/<?= $parameters; ?>" class="button-proefles">
										<i class="fa fa-calendar-o ani__all"></i>
										<span class="ani__all">Proefles aanvragen</span>
									</a><?php 
								endif; ?>  

							<div class="block__cursus__details">
								<div class="col-md-6 col-sm-6">
									<div class="block__cursus__detail">
										<label>Locatie(s):</label><?php
										$terms = wp_get_post_terms( get_the_ID(), 'locaties' );
										$locaties = array();
										$print_locaties = false;
										foreach ( $terms as $term ) {
											array_push( $locaties, get_field( 'woonpl-naam', 'locaties_'.$term->term_id ) );
										}
										$locaties = array_unique($locaties);
										foreach ( $locaties as $locatie ) { $print_locaties .= $locatie.", "; }
										echo substr( $print_locaties, 0, -2 ); ?>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="block__cursus__detail">
										<label>Docent(en):</label><?php
										$print_docenten = '';
										$docenten = get_field( 'related-docenten' );
										$docenten = array_unique($docenten);
										if ( count( $docenten ) ) { 
											foreach ( $docenten as $docent ) { 
												if ( get_field( 'roepnaam', $docent ) && get_field( 'docent-naam', $docent ) != 'nvt' ) {
													//echo get_field( 'roepnaam', $docent )." ".get_field( 'tus-vgsl' )." ".get_field( 'docent-naam', $docent );
													$print_docenten .= get_field( 'roepnaam', $docent )." ".get_field( 'tus-vgsl', $docent )." ".get_field( 'docent-naam', $docent ).", "; 		
												} elseif ( get_field( 'docent-naam', $docent ) != 'nvt' ) {
													//echo get_field( 'roepnaam', $docent )." ".get_field( 'docent-naam', $docent ); 	
													$print_docenten .= get_field( 'roepnaam', $docent )." ".get_field( 'tus-vgsl', $docent )." ".get_field( 'docent-naam', $docent ).", ";
												}
											}
											echo substr( $print_docenten, 0, -2 );
										} else {
											echo "-";
										} ?>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="block__cursus__detail">
										<label>Aantal lessen:</label>
										<?php echo bwh_get_min_lessen( get_the_ID(), true ); ?> lessen
									</div>
								</div>
								<?php if ( bwh_get_min_lesduur( get_the_ID() ) ) { ?>
								<div class="col-md-6 col-sm-6">
									<div class="block__cursus__detail">
										<label>Lesduur:</label>
										<?php echo bwh_get_min_lesduur( get_the_ID() ); ?>
									</div>
								</div>
								<?php } ?>
								<div class="col-md-6 col-sm-6">
									<div class="block__cursus__detail">
										<label>Frequentie:</label>
										<?php echo $frequenties[get_field( 'freq' )]; ?>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="block__cursus__detail">
										<label>Lesdagen:</label>
										<?php echo bwh_get_lesdagen( get_the_ID() ); ?>
									</div>
								</div>
								<?php //if (36 != bwh_get_max_lessen( get_the_ID() ) ) { ?>
								<div class="col-md-6 col-sm-6">
									<div class="block__cursus__detail">
										<label>Startdatum:</label>
										Zie lestijden en inschrijven<?php //echo bwh_get_startdatum( get_the_ID() ); ?>
									</div>
								</div>
								<?php //} ?>

								<div class="block__cursus__detail__price" id="price">
									<?php
									$tarieven = get_field( 'related-tarieven' );
									$les_tarief_btw = false;
									if( !empty($tarieven))
									foreach ( $tarieven as $tarief ) {
										$tarief_title = get_the_title($tarief);
										if ( strstr($tarief_title,'> 21') || strstr($tarief_title,'&gt; 21') ) {
											$les_tarief_btw = "<br/><small>Cursusbedrag is inclusief 21% BTW. Cursisten jonger dan 21 jaar betalen geen BTW.</small>";
										}
									}

									$total = str_replace( ',', '.', get_field( 'bruto-bdrg' ) );
									$split = $total / bwh_get_min_lessen( get_the_ID(), false );
									$total = number_format_i18n( $total, 2 );
									$split = number_format_i18n( $split, 2 );
									$materiaalbedrag = str_replace( ',', '.', get_field( 'mat-bdrg' ) );
									$materiaalbedrag = number_format_i18n( $materiaalbedrag, 2 );
									$tarieven_document = get_field('tarieven_document', 'option');
									?>
									Prijs per les&nbsp;&nbsp;<strong>&euro; <?php echo $split; //echo str_replace( '.', ',', money_format('%!n', $split ) ); ?></strong> &nbsp;&nbsp;|&nbsp;&nbsp; Totaal&nbsp;&nbsp;<strong>&euro; <?php echo $total; //echo str_replace( '.', ',', money_format('%!n', $total ) ); ?></strong>
									<span style="font-size:15px;"><?php if ($materiaalbedrag > 0 ) { echo"+ € ".$materiaalbedrag." materiaal"; } ?></span>
									<?php if ( $les_tarief_btw) : ?>
									<span class="block__cursus__detail__btw"><?php echo $les_tarief_btw; ?></span>
									<?php endif; ?>
									<?php if ( $tarieven_document) : ?>
									<br><a style="color:black;" target="_BLANK" href="<?=($tarieven_document['url']); ?>">Download tarieven</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="block__cursus__text styled__content">
							<h2 class="title title--centered title--red title--large"><?php echo bwh_cursus_title( get_the_title() ); ?></h2>
							<?php the_content(); ?>
							<button class="block__cursus__inschrijven" data-signup-type="les"><i class="fa fa-fw fa-pencil-square-o ani__all"></i><span class="ani__all">Lestijden & Inschrijven</span></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
