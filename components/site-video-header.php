<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12 video-header">

				  <div class="video-element">
   					<div class="video-fallback"></div>
						<div class="sound-toggle muted"></div>
						<video
							id="videoPlayer"
							autoplay="" loop="" muted="" playsinline=""
							src="https://player.vimeo.com/external/558127286.hd.mp4?s=7c12a7a11777c9325069d2fddbc57ebdd8283214&profile_id=174" >
						</video>
					</div>
					
          <div class="buttons">
						<a id="VideoButton1" href="/groepen/dans/"><em></em><span>Dans</span></a>
						<a id="VideoButton2" href="/groepen/kunst__design/"><em></em><span>Kunst</span></a>
						<a id="VideoButton3" href="/groepen/muziek/"><em></em><span>Muziek</span></a>
						<a id="VideoButton4" href="/groepen/theater/"><em></em><span>Theater</span></a>
					</div>
        </div>
      </div>
		</div>
	</div>
</div>