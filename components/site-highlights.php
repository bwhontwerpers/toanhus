<?php /* Highlights slider */ ?>

<div class="highlights__slider__wrapper">
	<div class="highlights__slider">
		<div class="highlights__slide highlights__slide--empty">&nbsp;</div>
		<div class="highlights__slide highlights__slide--empty">&nbsp;</div>
		<?php
		$highlights = get_field( 'home-highlights', 'option' );
		foreach ( $highlights as $highlight ) {
			?>
			<div class="highlights__slide">
				<a class="highlights__slide__inner" href="<?php echo get_permalink( $highlight['highlights-cursus'] ); ?>">
					<h3 class="highlights__slide__title title title--centered title--light title--shadow title--large ani__all"><?php echo $highlight['highlights-title']; ?></h3>
					<div class="highlights__slide__excerpt"><?php echo $highlight['highlights-excerpt']; ?></div>
					<i class="highlights__slide__arrow ani__all fa fa-arrow-right"></i>
					<div class="highlights__slider__visual__wrapper">
						<?php
						if ( has_post_thumbnail( $highlight['highlights-cursus'] ) ) {
							echo get_the_post_thumbnail( $highlight['highlights-cursus'], 'cursus-slider-visual', array( 'class' => 'highlights__slide__visual ani__all' ) );
						} else { 
							echo wp_get_attachment_image( get_field( 'global-aanbod-default-poster', 'option' ), 'cursus-slider-visual', false, array( 'class' => 'highlights__slide__visual ani__all' ) );
						}
						?>
					</div>
				</a>
			</div>	
			<?php
		}
		?>
	</div>
</div>

<div class="container container__slider__backdrop">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">
					<label class="highlights__slider__counter">
						<span class="label">Uitgelicht</span>
						<span class="count">...</span>
					</label>
				</div>
			</div>
		</div>
	</div>
</div>