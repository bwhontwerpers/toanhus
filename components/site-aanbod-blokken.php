<?php
  $aanbodTerms = get_terms( 'groepen', array( 'hide_empty' => 1, 'update_term_meta_cache' => false, 'parent' => 0 ) );
  foreach ( $aanbodTerms as $aanbodTerm ) {
  ?>
  <div class="col-md-3 col-sm-6">
    <div class="loop__cursus__item loop__cursus__item--category matchheight" onclick="document.location='<?php echo get_term_link($aanbodTerm->term_id); ?>';">
      <h3 class="title title--light title--shadow title--normal loop__cursus__title"><?php echo $aanbodTerm->name; ?></h3>
      <?php
      if ( get_field( 'groep-poster', 'groepen_'.$aanbodTerm->term_id ) ) { 
        echo wp_get_attachment_image( get_field( 'groep-poster', 'groepen_'.$aanbodTerm->term_id ), 'groep-loop-thumbnail', false, array( 'class' => 'loop__cursus__thumbnail ani__all' ) ); 
      }
      ?>
      
      <?php
      $subTerms = get_terms( 'groepen', array( 'hide_empty' => 1, 'parent' => $aanbodTerm->term_id ) );
      if ( count( $subTerms ) > 0 ) {
        ?>
        <ul class="loop__cursus__item__submenu ani__all hidden-sm hidden-xs">
        <?php
        foreach ( $subTerms as $subTerm ) { 
          ?>								
          <li><a href="<?php echo get_term_link( $subTerm->term_id ); ?>"><?php echo $subTerm->name; ?></a></li>
          <?php
        }
        ?>
        </ul>
        <?php
      }
      ?>								
    </div>
  </div>	
  <?php
  }
?>