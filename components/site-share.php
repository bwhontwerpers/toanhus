<div class="container">
	<div class="share__meta__wrapper">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="single__share__meta">
					<label class="share__meta__label ani__all">Pagina delen</label>
					<div class="share__meta__icons ani__all">
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url( get_the_permalink() ); ?>" target="_blank" class="share__meta__icon" title="Delen op Facebook">
							<i class="fa fa-facebook"></i>
						</a>
						<a href="https://twitter.com/home?status=Bekijk%20<?php echo esc_attr( get_the_title() ); ?>%20op%20<?php echo esc_url( get_the_permalink() ); ?>" target="_blank" class="share__meta__icon" title="Delen op Twitter">
							<i class="fa fa-twitter"></i>
						</a>
						<a href="mailto:?subject=&body=Bekijk%20<?php echo esc_attr( get_the_title() ); ?>%20op%20<?php echo esc_url( get_the_permalink() ); ?>" target="_blank" class="share__meta__icon" title="Delen via e-mail">
							<i class="fa fa-envelope"></i>
						</a>
						<a href="whatsapp://send?text=<?php echo esc_url( get_the_permalink() ); ?>" class="share__meta__icon share__meta__icon--smartphone" title="Delen via Whatsapp">
							<i class="fa fa-whatsapp"></i>
						</a>
						<a href="javascript:void(0);" class="share__meta__icon share__meta__icon--desktop clipboard" data-clipboard-text="<?php echo esc_url( get_the_permalink() ); ?>" title="Kopieer link naar klembord">
							<i class="fa fa-link"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>