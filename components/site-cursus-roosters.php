<?php global $tijden; global $frequenties; ?>
<div class="block__roosters__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__row__wrapper container__row__wrapper--padding">
					<div class="col-md-12">

						<div class="block__roosters__wrapper">
							<h2 class="title title--light"><?php echo bwh_cursus_title( get_the_title() ); ?></h2>
							<p>Je kunt hier je voorkeuren aangeven voor de lessen die je wilt volgen. Voor definitieve plaatsing en tijden wordt na je registratie altijd van te voren contact opgenomen.</p>

							<?php
									$roster_count = 0;
									$rosters = get_field( 'related-roosters' );
								//	print_r($rosters);
								//	echo "TESTING123".$rosters;
								?>
							<div class="block__roosters__roosters">
								<h3 class="title title--light title--small">Selecteer je locatie/rooster</h3>
								<ul class="block__roosters__list">
									<?php
									$roster_count = 0;
									$rosters = get_field( 'related-roosters' );
								//	echo "TESTING123".$rosters;
									/* Collect rosters and sort them by timestamp (startdatum + aanvang) before processing */
									$unrosters_sorted = $rosters;
									$sorted_rosters = array();
									foreach ( $unrosters_sorted as $unsorted_roster ) {
										if ( 1 == get_field( 'Op_website_tonen', $unsorted_roster ) ) {
											$timestamp_roster = bwh_convert_date( get_field( 'startdatum', $unsorted_roster ) ) . " " .date('H:i', strtotime( get_field( 'aanvang', $unsorted_roster ) ) ).$roster_count;
											$sorted_rosters[$timestamp_roster] = $unsorted_roster;
										}
										$roster_count++;
									}

									ksort($sorted_rosters);


									$roster_count = 0;
									/* Continue processing rosters using sorted_rosters array */

									$tarief = (float)get_field( 'bruto-bdrg');
									foreach ( $sorted_rosters as $roster ) { 
										if ( 1 == get_field( 'Op_website_tonen', $roster ) ) {
											$roster_count++;
											$location_id = get_field( 'id-lokaal', $roster );
											$locations = get_terms( 'locaties', array( 'hide_empty' => 1 ) );
											foreach ( $locations as $location ) { 
												if ( get_field( 'id-lokaal', 'locaties_'.$location->term_id ) == $location_id ) {
													$print_location_name = get_field( 'woonpl-naam', 'locaties_'.$location->term_id );
													$print_location_id = $location->term_id;
												}
											}
											?>
											<li>
												<input value="" type="radio" name="blokvorm" id="blokvorm-<?php echo $roster_count; ?>" data-roster-id="<?php echo $roster; ?>" data-roster-startdate="<?php the_field( 'startdatum', $roster ); ?>" data-roster-price="" data-roster-desc="<?php the_field( 'startdatum', $roster ); ?> - <?php the_field( 'lesdag', $roster ); ?> van <?php echo date('H:i', strtotime( get_field( 'aanvang', $roster ) ) ); ?> tot <?php echo date('H:i', strtotime( get_field( 'eindtijd', $roster ) ) ); ?>" />
												<label for="blokvorm-<?php echo $roster_count; ?>">
													<span class="roosters__list--location"><i class="fa fa-map-marker"></i> <?php echo $print_location_name; ?></span>
													<span class="roosters__list--time"><i class="fa fa-clock-o"></i> <?php the_field( 'lesdag', $roster ); ?> van <?php echo date('H:i', strtotime( get_field( 'aanvang', $roster ) ) ); ?> tot <?php echo date('H:i', strtotime( get_field( 'eindtijd', $roster ) ) ); ?></span>
													<span class="roosters__list--date"><i class="fa fa-calendar"></i> <?php echo date('d/m/Y', strtotime( bwh_convert_date( get_field( 'startdatum', $roster ) ) ) ); ?></span>
													<span class="roosters__list--duration"><i class="fa fa-clock-o"></i> <?php echo $tijden[get_field( 'lestijd', $roster )]; ?></span>
													<span class="roosters__list--lessons"><?php the_field( 'lessen', $roster ); ?> lessen</span>
													<span class="roosters__list--repeative"><?php echo $frequenties[get_field( 'freq', $roster )]; ?></span>
												</label>
											</li>
											<?php
										}
									}
									?>
								</ul>
							</div>

							<div class="roosters__price">&euro; <?php echo money_format('%!n', $tarief ); ?></div>
							<button class="button button--advanced button--nextform" disabled="disabled"><i class="fa fa-fw fa-arrow-right ani__all"></i> <span class="ani__all">Volgende stap 1/3</span></button>
							<!--<button class="button button--advanced button--return"><i class="fa fa-fw fa-arrow-left ani__all"></i> <span class="ani__all">Kies een ander rooster</span></button>-->

						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
