<?php
  // Depandacy: $aanbod en $groep from taxonomy-groep.php
?>

<div class="block__category__container aanbod-header-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__row__wrapper no-gutter aanbod-header-content-background">
					<div class="col-md-6 col-sm-6">
						<div class="block__category__title">
						<?php 
							$groepname = ''; // used for auto-select gravityform
							$subgroepname = '';

							$proeflessen = [
								['groep' => 'dans', 'subgroepen' => 'all'],
								['groep' => 'theater', 'subgroepen' => 'all'],
								['groep' => 'muziek', 'subgroepen' => ['groepen','instrumenten']],
							];

							if( !$groep->is_parent ) : 
								$parent = $aanbod->getTerm($groep->term->parent); 
								$groepname = strtolower($parent->name);
								$subgroepname = strtolower($groep->term->name); ?>
              	<a class="button-parent" href="<?= get_term_link($parent->term_id); ?>"><?= $parent->name; ?></a><?php 
							else: 
								$groepname = strtolower($groep->term->name);
							endif;

							// echo "Groep {$groepname} en subgroep {$subgroepname}";
							$proefles = false;
							foreach( $proeflessen as $checkgroep ) {
								if( $checkgroep['groep'] === $groepname ) {
									$proefles = ( ($checkgroep['subgroepen'] == 'all') || in_array($subgroepname, $checkgroep['subgroepen']) );
									break;
								}
							}
							if( $proefles ) {
								$parameters = ucfirst($groepname);
								if( !empty($subgroepname) ) $parameters .= '-' . ucfirst($subgroepname);
							}
						?>
						<h2 class="title title--light title--medium title--large"><?= $groep->term->name; ?></h2><?php 
						if ( $proefles ) : ?>
							<a href="/proefles-aanvragen/?discipline=<?= $parameters; ?>" class="button-proefles-header button-yellow">Proefles aanvragen</a><?php 
						endif; ?>   
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="block__category__text">
							<?= $groep->intro; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>