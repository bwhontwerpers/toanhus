<?php /* Agenda listing block */ ?>

<div class="block__agenda matchheight">
	<h2 class="title title--light">Agenda</h2>
	<ul class="agenda__articles">
		<?php
		$eventsQuery = new WP_Query( array( 'order'=>'ASC', 'category_name' => 'agenda', 'no_found_rows' => true, 'update_post_term_cache' => false, 'update_post_meta_cache' => false, 'post_type' => 'post', 'posts_per_page' => 3, 'post_status' => 'publish', 'date_query' => array( array( 'after' => 'yesterday' ) ) ) );
		while ( $eventsQuery->have_posts() ) { 
			$eventsQuery->the_post();
			?>
			<li class="agenda__articles__article">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<time datetime="<?php echo date_i18n( 'Y-m-d h:i', strtotime($post->post_date_gmt), true ); ?>">
					<span class="day ani__all"><?php echo date_i18n( 'D', strtotime($post->post_date_gmt), true ); ?></span>
					<span class="date ani__all"><?php echo date_i18n( 'd', strtotime($post->post_date_gmt), true ); ?></span>
					<span class="month ani__all"><?php echo date_i18n( 'M', strtotime($post->post_date_gmt), true ); ?></span>
					
				</time>
				<h3><?php the_title(); ?></h3>
				<?php the_excerpt(); ?>
				</a>
			</li>
			<?php
		} wp_reset_query();
		?>
	</ul>		
</div>
