<?php
	 global $tijden; global $frequenties; ?>
<div class="block__roosters__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__row__wrapper container__row__wrapper--padding">
					<div class="col-md-12">
						
						<div class="block__roosters__wrapper">
							<h2 class="title title--light"><?php the_title(); ?></h2>
							<p>Je kunt hier je voorkeuren aangeven voor de lessen die je wilt volgen. Voor definitieve plaatsing en tijden wordt na je registratie altijd van te voren contact opgenomen.</p>
							<?php 											//	echo "roostersschema".get_field( 'lessen' );
?>
							<div class="block__roosters__roosters block__roosters__roosters--lessen block__roosters__list">
								<?php echo do_shortcode('[gravityform id=4 title=false description=false ajax=true tabindex=20 field_values="dynamic_lessen='.get_field( 'lessen' ).'"]'); ?>
							</div>
							
							<div class="roosters__price">&euro; 0,00</div>
							<button class="button button--advanced button--nextform" disabled="disabled"><i class="fa fa-fw fa-arrow-right ani__all"></i> <span class="ani__all">Volgende stap 1/3</span></button>
							<!--<button class="button button--advanced button--nextform" disabled="disabled"><i class="fa fa-fw fa-arrow-right ani__all"></i> <span class="ani__all">Volgende stap 1/3</span></button>-->
							<!--<button class="button button--advanced button--return"><i class="fa fa-fw fa-arrow-left ani__all"></i> <span class="ani__all">Kies een ander rooster</span></button>-->
							
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>