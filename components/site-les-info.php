<?php /* Les info blocks */ ?>

<div class="block__cursus__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__row__wrapper">
					<div class="col-md-6">
						<div class="block__cursus__wrapper">
							<button class="block__cursus__inschrijven" data-signup-type="les"><i class="fa fa-fw fa-pencil-square-o ani__all"></i><span class="ani__all">Lestijden & Inschrijven</span></button>
							<?php 
								//
								//	COPY "PROEFLES-BUTTON-CHANGES" TOT SITE-LES-INFO!
								//
								$proefles = false;
								$terms = wp_get_post_terms( get_the_ID(), 'groepen' );
								$has_proeflessen = [
									'dans' => ['discipline' => 'Dans', 'parameter' => 'dans'],
									'instrumenten' => ['discipline' => 'Muziek-Instrumenten', 'parameter' => 'instrument'],
									'groepen' => ['discipline' => 'Muziek-Groepen', 'parameter' => 'groepen'],
									'theater' => ['discipline' => 'Theater', 'parameter' => 'theater']
								];

								foreach( $terms as $term ) {
									if( array_key_exists($term->slug, $has_proeflessen) ) {
										$proefles = $has_proeflessen[$term->slug];
										break;
									} elseif( !empty($term->parent) ) {
										$parent = get_term_by('term_id', $term->parent, 'groepen');
										if( array_key_exists($parent->slug, $has_proeflessen) ) {
											$proefles = $has_proeflessen[$parent->slug];
											break;
										}
									}
								}

								if ( false !== $proefles ) : 
									global $post;
									$parameters = "?discipline={$proefles['discipline']}&{$proefles['parameter']}={$post->post_name}"; ?>
									<a href="/proefles-aanvragen/<?= $parameters; ?>" class="button-proefles">
										<i class="fa fa-calendar-o ani__all"></i>
										<span class="ani__all">Proefles aanvragen</span>
									</a><?php 
								endif; ?>  

							<div class="block__cursus__details">
								<div class="col-md-6">
									<label>Locatie(s):</label>
									<?php
									$terms = wp_get_post_terms( get_the_ID(), 'locaties' );
									$locaties = array();
									$print_locaties = false;
									
									foreach ( $terms as $term ) {
										array_push( $locaties, get_field( 'woonpl-naam', 'locaties_'.$term->term_id ) );
									}
									$locaties = array_unique($locaties);
									foreach ( $locaties as $locatie ) { $print_locaties .= $locatie.", "; }
									echo substr( $print_locaties, 0, -2 );
									?>
								</div>
								<div class="col-md-6">
									<label>Docent(en):</label>
									<?php
									$docenten = get_field( 'related-docenten' );
									$docenten = array_unique($docenten);
									$print_docenten = "";
									if ( count( $docenten ) ) { 
										foreach ( $docenten as $docent ) { 
											if ( get_field( 'roepnaam', $docent ) && get_field( 'docent-naam', $docent ) != 'nvt' ) {
												$print_docenten .= get_field( 'roepnaam', $docent )." ".get_field( 'tus-vgsl', $docent )." ".get_field( 'docent-naam', $docent ).", "; 		
											} elseif ( get_field( 'docent-naam', $docent ) != 'nvt' ) {
												$print_docenten .= get_field( 'roepnaam', $docent )." ".get_field( 'tus-vgsl', $docent )." ".get_field( 'docent-naam', $docent ).", ";
											} else {

											}
										}
										echo substr( $print_docenten, 0, -2 );
									} else {
										echo "-";
									}
									?>
								</div>
							</div>
							<div class="block__cursus__info <?php if ('hafa' == get_field( 'inkcat' ) ) { ?>block__cursus__info--hafa<?php } ?>">
								<h2 class="title title--light">Prijs berekenen<a class="block__cursus__reset ani__all" href="javascript:toanhus_reset_calculator();"><i class="fa fa-fw fa-refresh ani__all"></i></a></h2>
								<?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true tabindex=1 field_values="dynamic_cursusid='.get_the_ID().'&dynamic_lessen='.get_field( 'lessen' ).'"]'); ?>
								<div class="price-wrapper">
									<span id="price">Bereken de prijs voor deze cursus</span><?php
										$tarieven_document = get_field('tarieven_document', 'option');
										if ( $tarieven_document) : ?>
											<br><a style="color:black;" target="_BLANK" href="<?=($tarieven_document['url']); ?>">Download tarieven</a><?php 
										endif; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="block__cursus__text styled__content">
							<h2 class="title title--centered title--red title--large"><?php echo bwh_cursus_title( get_the_title() ); ?></h2>
							<?php the_content(); ?>
							<!-- <button class="block__cursus__inschrijven" data-signup-type="les"><i class="fa fa-fw fa-pencil-square-o ani__all"></i><span class="ani__all">Lestijden & Inschrijven</span></button> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>