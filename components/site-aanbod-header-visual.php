<?php
  // Depandacy: $aanbod en $groep from taxonomy-groep.php
?>
<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__header__visual__wrapper aanbod-header-visual">
					<button class="container__header__back" onclick="window.history.go(-1);"><i class="ani__all fa fa-chevron-left fa-fw"></i></button>
          <div class="aanbod-header" style="background-image: url(<?= $groep->image[0]; ?>);"></div>
        </div>
			</div>
		</div>
	</div>
</div>