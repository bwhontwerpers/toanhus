<?php /* Cursus finder */ ?>

<div class="container block__aanbod__finder__wrapper">
	<form method="post" action="/aanbod/" name="block__aanbod__finder__form">
	<div class="row no-gutter">
		<div class="col-md-12">
			<div class="block__aanbod__finder">
				<h2 class="title title--light">Ons aanbod</h2>
				
				<button class="comp__close__finder"><i class="fa fa-times ani__all"></i></button>
									
				<div class="aanbod__finder__filters no-gutter">
					<div class="col-md-4 col-sm-6">
						<h3>Discipline</h3>
						<?php $disciplines = get_terms( 'groepen', array( 'hide_empty' => true, 'hierarchical' => true, 'parent' => 0 ) ); ?>
						<ul class="aanbod__finder__filters__list">
							<?php foreach ( $disciplines as $discipline ) { ?>
								<li class="aanbod__finder__filters__item aanbod__finder__filters__item--parent" data-parent-id="<?php echo $discipline->term_id; ?>">
									<input type="checkbox" name="filter-groepen[]" id="groepen-<?php echo $discipline->term_id; ?>" value="<?php echo $discipline->term_id; ?>" />
									<label class="ani__all" for="groepen-<?php echo $discipline->term_id; ?>"><?php echo $discipline->name; ?></label>
								</li>
								<?php 
								$subdisciplines = get_terms( 'groepen', array( 'hide_empty' => true, 'hierarchical' => true, 'parent' => $discipline->term_id ) );
								foreach ( $subdisciplines as $subdisciple ) { 
								?>
									<li class="aanbod__finder__filters__item aanbod__finder__filters__item--sub" data-child-parent="<?php echo $discipline->term_id; ?>">
										<input type="checkbox" name="filter-groepen[]" id="groepen-<?php echo $subdisciple->term_id; ?>" value="<?php echo $subdisciple->term_id; ?>" />
										<label class="ani__all" for="groepen-<?php echo $subdisciple->term_id; ?>"><?php echo $subdisciple->name; ?></label>
									</li>																	
								<?php } ?>
							<?php } ?>
						</ul>
					</div>
					<div class="col-md-4 col-sm-6">
						<h3>Leeftijd</h3>
						<?php $ages = get_terms( 'leeftijden', array( 'hide_empty' => 1, 'parent'=>1481 ) ); ?>
						<ul class="aanbod__finder__filters__list">
							<?php foreach ( $ages as $age ) { ?>
							<li class="aanbod__finder__filters__item">
								<input type="checkbox" name="filter-leeftijden[]" id="leeftijden-<?php echo $age->term_id; ?>" value="<?php echo $age->term_id; ?>" />
								<label class="ani__all" for="leeftijden-<?php echo $age->term_id; ?>"><?php echo $age->name; ?></label>
							</li>
							<?php } ?>
						</ul>
					</div>
					<div class="col-md-4 col-sm-12">
						<h3>Locaties</h3>
						<?php $locations = get_terms( 'locaties', array( 'hide_empty' => 1, 'parent' => 0 ) ); ?>
						<ul class="aanbod__finder__filters__list">
							<?php foreach ( $locations as $location ) { ?>
							<li class="aanbod__finder__filters__item">
								<input type="checkbox" name="filter-locaties[]" id="locaties-<?php echo $location->term_id; ?>" value="<?php echo $location->term_id; ?>" />
								<label class="ani__all" for="locaties-<?php echo $location->term_id; ?>"><?php echo $location->name; ?></label>
							</li>
							<?php } ?>
						</ul>
					</div>
					
				</div>
				
				<button class="comp__start__finder comp__start__finder--finder comp__start__finder--disabled" type="submit"><i class="fa fa-search ani__all"></i><span class="ani__all">Filter aanbod!</span></button>
	
			</div>
		</div>
	</div>
	</form>
</div>
