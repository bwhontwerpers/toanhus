<?php
/**
 * Template Name: Aanbod template
 *
 * @package WordPress
 * @subpackage toanhus
 * @since toanhus 1.0
 */
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { $ajax = true; } else { $ajax = false; }
if ($ajax != true) { get_header(); }

?>

<?php while ( have_posts() ) { the_post(); ?>

<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__header__visual__wrapper">
					<button class="container__header__back" onclick="window.history.go(-1);"><i class="ani__all fa fa-chevron-left fa-fw"></i></button>
					<?php
					if ( get_field( 'header-visual' ) ) { $header = get_field( 'header-visual' ); $header = $header['ID']; } 
					else { $header = get_field( 'global-article-header', 'option' ); }
					echo wp_get_attachment_image( $header, 'page-header-visual', false, array( 'class' => 'container__header__visual' ) );
					?>
					<h2 class="news__article__title"><?php echo bwh_cursus_title( get_the_title() ); ?></h2>
				</div>
			</div>
		</div>
	</div>
</div>

<!--<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<?php
								$group = get_page_by_path( 'in-het-onderwijs' );
								$children = wp_list_pages( 'title_li=&child_of='.$group->ID.'&echo=0&depth=1' );
								if ( $children) : ?>
								    <ul class="subpage__navigation">
								        <?php echo $children; ?>
								    </ul>
								<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>-->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">
					<div class="row">
						<div class="col-xs-12 col-md-3 col-md-offset-1 news__article--date">
							<?php
								$aanbod = get_page_by_path( 'in-het-onderwijs/aanbod' );
								$children = wp_list_pages( 'title_li=&child_of='.$aanbod->ID.'&echo=0&depth=2' );
								if ( $children) : ?>
								    <ul class="subpage__navigation left">
								        <?php echo $children; ?>
								    </ul>
							<?php endif; ?>
						</div>
						<div class="col-md-6">
							<?php the_content(); ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-10 col-md-offset-1 spacing">
							<div class="row">
							<?php 

							$args = array(
							    'post_type'      => 'page',
							    'posts_per_page' => -1,
							    'post_parent'    => $post->ID,
							    'order'          => 'ASC',
							    'orderby'        => 'menu_order'
							 );
							
							$parent = new WP_Query( $args );
							
							if ( $parent->have_posts() ) : ?>
							
							    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
							        
							        <div class="col-xs-12 col-sm-6 col-lg-3">
												<a class="loop__cursus__item matchheight" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
													<h3 class="title title--light title--shadow title--normal loop__cursus__title"><?php echo bwh_cursus_title( get_the_title() ); ?></h3>
													
													<?php 

													$image = get_field('header-visual');
													
													if( !empty($image) ): 
													
														// vars
														$url = $image['url'];
														$title = $image['title'];
														$alt = $image['alt'];
														$caption = $image['caption'];
													
														// thumbnail
														$size = 'thumbnail';
														$thumb = $image['sizes'][ $size ];
														$width = $image['sizes'][ $size . '-width' ];
														$height = $image['sizes'][ $size . '-height' ]; ?>
													
															<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="loop__cursus__thumbnail ani__all" />
	
													
													<?php endif; ?>
													
												</a>
											</div>
							
							    <?php endwhile; ?>
							
							<?php endif; wp_reset_query(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
							
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">&nbsp;</div>
			</div>
		</div>
	</div>
</div>


<?php } ?>

<?php if ($ajax != true) { get_footer(); } ?>