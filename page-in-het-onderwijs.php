<?php
/* Single cursus template */
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { $ajax = true; } else { $ajax = false; }
if ($ajax != true) { get_header(); }
?>

<?php while ( have_posts() ) { the_post(); ?>

<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__header__visual__wrapper">
					<button class="container__header__back" onclick="window.history.go(-1);"><i class="ani__all fa fa-chevron-left fa-fw"></i></button>
					<?php
					if ( get_field( 'header-visual' ) ) { $header = get_field( 'header-visual' ); $header = $header['ID']; }
					else { $header = get_field( 'global-article-header', 'option' ); }
					echo wp_get_attachment_image( $header, 'page-header-visual', false, array( 'class' => 'container__header__visual' ) );
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="block__category__container education-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__row__wrapper no-gutter">
					<div class="col-md-6 col-sm-6">
						<div class="block__category__title">
							<h2 class="title title--light title--medium title--large"><?php the_title(); ?></h2>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="block__category__text">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<h2> Aanbod </h2>
							<div class="row">
							<?php

							$aanbod = get_page_by_path( 'in-het-onderwijs/aanbod' );

							$args = array(
							    'post_type'      => 'page',
							    'posts_per_page' => -1,
							    'post_parent'    => $aanbod->ID,
							    'order'          => 'ASC',
							    'orderby'        => 'menu_order'
							 );


							$parent = new WP_Query( $args );

							if ( $parent->have_posts() ) : ?>

							    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

							        <div class="col-xs-12 col-sm-6 col-lg-3">
												<a class="loop__cursus__item matchheight" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
													<h3 class="title title--light title--shadow title--normal loop__cursus__title"><?php the_title(); ?></h3>

													<?php

													$image = get_field('header-visual');

													if( !empty($image) ):

														// vars
														$url = $image['url'];
														$title = $image['title'];
														$alt = $image['alt'];
														$caption = $image['caption'];

														// thumbnail
														$size = 'thumbnail';
														$thumb = $image['sizes'][ $size ];
														$width = $image['sizes'][ $size . '-width' ];
														$height = $image['sizes'][ $size . '-height' ]; ?>

															<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="loop__cursus__thumbnail ani__all" />


													<?php endif; ?>

												</a>
											</div>

							    <?php endwhile; ?>

							<?php endif; wp_reset_query(); ?>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">&nbsp;</div>
			</div>
		</div>
	</div>
</div>


<?php } ?>

<?php if ($ajax != true) { get_footer(); } ?>