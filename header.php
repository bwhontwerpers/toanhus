<?php header('X-UA-Compatible: IE=edge,chrome=1'); setlocale(LC_ALL, 'nl_NL'); ?><!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 9]><html class="ie ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if !(IE 7) | !(IE 8) | !(IE 9) ]><!--><html <?php language_attributes(); ?>><!--<![endif]-->
<head>
	<meta name="google-site-verification" content="3M_IuvI0iTei-CPVdqCUQG7G9lXJaZNzKiW_edQvK-c" />
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="apple-mobile-web-app-capable" content="yes" />

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-JV17JCXSDF"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-JV17JCXSDF');
</script>
	
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png?v=allLyKBwKo">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png?v=allLyKBwKo">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png?v=allLyKBwKo">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png?v=allLyKBwKo">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png?v=allLyKBwKo">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png?v=allLyKBwKo">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png?v=allLyKBwKo">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png?v=allLyKBwKo">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png?v=allLyKBwKo">
	<link rel="icon" type="image/png" href="/favicon-32x32.png?v=allLyKBwKo" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-194x194.png?v=allLyKBwKo" sizes="194x194">
	<link rel="icon" type="image/png" href="/favicon-96x96.png?v=allLyKBwKo" sizes="96x96">
	<link rel="icon" type="image/png" href="/android-chrome-192x192.png?v=allLyKBwKo" sizes="192x192">
	<link rel="icon" type="image/png" href="/favicon-16x16.png?v=allLyKBwKo" sizes="16x16">
	<!-- <link rel="manifest" href="/manifest.json?v=allLyKBwKo"> -->
	<link rel="mask-icon" href="/safari-pinned-tab.svg?v=allLyKBwKo" color="#522a81">
	<link rel="shortcut icon" href="/favicon.ico?v=allLyKBwKo">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png?v=allLyKBwKo">
	<meta name="theme-color" content="#522a81">	
	
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	
	<?php wp_head(); ?>

	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjDm6273wigQ7Jgx0HVZ6NEcdHGAu503U"
  type="text/javascript"></script>

</head>

<body <?php body_class('ani__body'); ?>>

	<?php get_template_part( 'components/site', 'background' ); ?>
	<?php get_template_part( 'components/site', 'canvas-menu' ); ?>

	<div class="dynamic" id="dynamic">
		<?php get_search_form(); ?>
		<header class="container">
			<div class="row">
				<div class="header__wrapper ani__all">
					<div class="hidden-md visible-sm visible-xs hidden-md col-sm-6 col-xs-6 col-sm-offset-3">
						<?php if ( is_front_page() ) { ?>
							<h1><?php bwh_load_image('th_header_logo.svg', 'SEO', 'header__wrapper__logo', true); ?><!--<img src="<?php echo IMAGEFOLDER ?>/svg/logofull_ani.svg">--></h1>
						<?php } else { ?>
							<a href="/"><?php bwh_load_image('th_header_logo.svg', 'SEO', 'header__wrapper__logo', true); ?></a>
						<?php } ?>
						<span class="header__wrapper__tagline hidden-xs">Muziek, Dans, Theater, Kunst &amp; Design</span>
					</div>
					<div class="col-md-4 col-sm-6 hidden-xs">
						<?php wp_nav_menu( array('theme_location' => 'topleft', 'container' => false, 'menu_class' => 'header__menu header__menu--left' ) ); ?>
					</div>
					<div class="col-md-4 hidden-sm hidden-xs logofull">
						<?php if ( is_front_page() ) { ?>
							<h1><?php bwh_load_image('th_header_logo.svg', 'SEO', 'header__wrapper__logo', true); ?><!--<img src="<?php echo IMAGEFOLDER ?>/svg/logofull_ani.svg">--></h1>
						<?php } else { ?>
							<a href="/"><?php bwh_load_image('th_header_logo.svg', 'SEO', 'header__wrapper__logo', true); ?></a>
						<?php } ?>
						<span class="header__wrapper__tagline">Muziek, Dans, Theater, Kunst &amp; Design</span>
					</div>
					<div class="col-md-4 col-sm-6 hidden-xs">
						<button class="header__search__button"><i class="fa fa-fw fa-search"></i></button>
						<?php wp_nav_menu( array('theme_location' => 'topright', 'container' => false, 'menu_class' => 'header__menu header__menu--right' ) ); ?>
					</div>
					<div class="col-xs-6 hidden-sm hidden-md hidden-lg">
						<button class="header__menu__button"><i class="fa fa-fw fa-bars"></i></button>
						<button class="header__search__button"><i class="fa fa-fw fa-search"></i></button>
						<button class="header__phone__button" onclick="document.location='tel:<?php echo str_replace( array( '-', ' ' ), array( '', '' ), get_field( 'global-phone', 'option' ) ); ?>';"><i class="fa fa-fw fa-phone"></i></button>
					</div>
						<div class="social__menu social__menu--right">
							<a href="https://www.instagram.com/toanhus_insta/"><i class="fa fa-instagram"></i></a>&nbsp;
							<a href="https://www.facebook.com/Toanhus"><i class="fa fa-facebook"></i></a>
						</div>
				</div>
			</div>
		</header>
		
		<?php get_template_part( 'components/site', 'announcements' ); ?>
	
	