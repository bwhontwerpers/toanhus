<?php
/* Template name: Proefles pagina */
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { $ajax = true; } else { $ajax = false; }
if ($ajax != true) { get_header(); }

// Dynamic populate the GravityForm "Inschrijven proefles"
// https://docs.gravityforms.com/dynamically-populating-drop-down-fields/
$gfId = RGFormsModel::get_form_id('Inschrijven proefles');
if( empty($gfId) ) die('Formulier niet gevonden, controleer titel en id.');
add_filter( "gform_pre_render_$gfId", 'populate_posts' );
add_filter( "gform_pre_validation_$gfId", 'populate_posts' );
add_filter( "gform_pre_submission_filter_$gfId", 'populate_posts' );
add_filter( "gform_admin_pre_render_$gfId", 'populate_posts' );

function populate_posts( $form ) {
	foreach ( $form['fields'] as $field ) {
		if( $field->type != 'select' ) continue;

		$populate = false;

		// Tweede select "Cursus" vullen als deze getoond worden (adhv keuze).
		switch( $field->inputName ) {
			case 'dans' : 
				$groepterm = get_term_by('name', 'dans', 'groepen');
				$populate = AanbodService::i()->getAanbodCourses($groepterm->term_id);
				break;
			case 'instrument' : 
				$groepterm = get_term_by('name', 'instrumenten', 'groepen');
				$populate = AanbodService::i()->getAanbodCourses($groepterm->term_id);
				break;
			case 'groepen' : 
				$groepterm = get_term_by('name', 'groepen', 'groepen');
				$populate = AanbodService::i()->getAanbodCourses($groepterm->term_id);
				break;
			case 'theater' : 
				$groepterm = get_term_by('name', 'theater', 'groepen');
				$populate = AanbodService::i()->getAanbodCourses($groepterm->term_id);
				break;
		}

		if( $populate == false ) 
			continue;

		// Vullen maar
		$choices = [];
		foreach ( $populate as $entry ) {
			$choices[] = [
				'text' => $entry['title'], 
				'value' => $entry['id']
			];
			$field->choices = $choices;
		}
	}
	return $form;
}
?>

<?php while ( have_posts() ) { the_post(); ?>

	<div class="container__header__visual__container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="container__header__visual__wrapper">
					<button class="container__header__back" onclick="window.history.go(-1);"><i class="ani__all fa fa-chevron-left fa-fw"></i></button>
					<?php
					if ( get_field( 'header-visual' ) ) { $header = get_field( 'header-visual' ); $header = $header['ID']; } 
					else { $header = get_field( 'global-article-header', 'option' ); }
					echo wp_get_attachment_image( $header, 'page-header-visual', false, array( 'class' => 'container__header__visual' ) );
					?>
					<h2 class="news__article__title"><?php echo bwh_cursus_title( get_the_title() ); ?></h2>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
						<?php wp_nav_menu( array( 'menu' => '1460', 'menu_class' => 'subpage__navigation', 'container' => false, ) ); ?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->

<div class="container page-content-wrapper-single">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="matchheight styled__content">

							<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
							
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-12">&nbsp;</div>
			</div>
		</div>
	</div>
</div>

<?php get_template_part( 'components/site', 'share' ); ?>

<?php } ?>

<?php if ($ajax != true) { get_footer(); } ?>