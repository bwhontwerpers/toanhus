<?php
/* Template name: Startpagina */
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { $ajax = true; } else { $ajax = false; }
if ($ajax != true) { get_header(); }
?>

<?php get_template_part( 'components/site', 'video-header' ); ?>
<?php get_template_part( 'components/site', 'highlights' ); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="container__row__wrapper">
				<div class="col-md-6">
					<?php get_template_part( 'components/site', 'news' ); ?>
				</div>
				<div class="col-md-6">
					<?php get_template_part( 'components/site', 'agenda' ); ?>
				</div>
			</div>
			<div class="container__row__wrapper">
				<?php get_template_part( 'components/site', 'aanbod-blokken' ); ?>
			</div>
		</div>
	</div>
</div>

<?php
if ($ajax != true) { get_footer(); } 
?>